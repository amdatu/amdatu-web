/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.util.test.download;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_SERVLET_INIT_PARAM_PREFIX;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_SERVLET_PATTERN;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Dictionary;
import java.util.Hashtable;

import javax.servlet.Servlet;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.osgi.service.log.LogService;

import com.github.kevinsawicki.http.HttpRequest;

public class DownloadHandlerTest {
    private String m_urlBase;
    private TestDownloadHandler m_resProvider;

    @Before
    public void before() {
        String httpPort = System.getProperty("org.osgi.service.http.port", "8080");
        m_urlBase = String.format("http://localhost:%s/res/", httpPort);

        m_resProvider = new TestDownloadHandler();

        Dictionary<String, Object> props = new Hashtable<>();
        props.put(HTTP_WHITEBOARD_SERVLET_PATTERN, "/res/*");
        props.put(HTTP_WHITEBOARD_SERVLET_INIT_PARAM_PREFIX + "debug", "true");
        props.put("type", "junit-test");

        configure(this)
            .add(createComponent()
                .setInterface(Servlet.class.getName(), props)
                .setImplementation(new DownloadServlet(m_resProvider))
                .add(createServiceDependency().setService(LogService.class).setRequired(false)))
            .apply();
    }

    @After
    public void after() {
        cleanUp(this);
    }

    @Test
    public void testCreateResourceServletForDefaultContext() throws Exception {
        File f = createTempFile("hello world!");

        m_resProvider.addFile(f, "text/plain");

        HttpRequest req = HttpRequest.get(m_urlBase + f.getName());

        assertEquals(200, req.code());
        assertEquals("hello world!", req.body());
    }

    @Test
    public void testCreateEmptyResourceServletForDefaultContext() throws Exception {
        File f = createTempFile("");

        m_resProvider.addFile(f, "text/plain");

        HttpRequest req = HttpRequest.get(m_urlBase + f.getName());

        assertEquals(204, req.code());
        assertEquals("", req.body());
    }

    @Test
    public void testCreateResourceServletWithPartialRange() throws Exception {
        File f = createTempFile("hello world!");

        m_resProvider.addFile(f, "text/plain");

        HttpRequest req;
        String body;

        req = HttpRequest.get(m_urlBase + f.getName()).header("Range", "bytes=1-4");
        body = req.body();
        assertEquals("ello", body);

        req = HttpRequest.get(m_urlBase + f.getName()).header("Range", "bytes=0-0,-1");
        body = req.body();
        assertTrue(body.contains("Content-Range: bytes 0-0/12\r\nh\r\n"));
        assertTrue(body.contains("Content-Range: bytes 11-11/12\r\n!\r\n"));
    }

    private File createTempFile(String content) throws IOException {
        Path f = Files.createTempFile("rs", ".txt");
        Files.write(f, content.getBytes(UTF_8));
        File file = f.toFile();
        file.deleteOnExit();
        return file;
    }

}