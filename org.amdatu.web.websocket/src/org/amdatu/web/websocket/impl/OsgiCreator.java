/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
//
//  ========================================================================
//  Copyright (c) 1995-2017 Mort Bay Consulting Pty. Ltd.
//  ------------------------------------------------------------------------
//  All rights reserved. This program and the accompanying materials
//  are made available under the terms of the Eclipse Public License v1.0
//  and Apache License v2.0 which accompanies this distribution.
//
//      The Eclipse Public License is available at
//      http://www.eclipse.org/legal/epl-v10.html
//
//      The Apache License v2.0 is available at
//      http://www.opensource.org/licenses/apache2.0.php
//
//  You may elect to redistribute this code under either of these licenses.
//  ========================================================================
//
package org.amdatu.web.websocket.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.websocket.Extension;
import javax.websocket.Extension.Parameter;
import javax.websocket.server.ServerEndpointConfig;

import org.eclipse.jetty.http.pathmap.PathSpec;
import org.eclipse.jetty.http.pathmap.UriTemplatePathSpec;
import org.eclipse.jetty.util.StringUtil;
import org.eclipse.jetty.websocket.api.extensions.ExtensionConfig;
import org.eclipse.jetty.websocket.api.extensions.ExtensionFactory;
import org.eclipse.jetty.websocket.common.scopes.WebSocketContainerScope;
import org.eclipse.jetty.websocket.jsr356.JsrExtension;
import org.eclipse.jetty.websocket.jsr356.endpoints.EndpointInstance;
import org.eclipse.jetty.websocket.jsr356.server.BasicServerEndpointConfig;
import org.eclipse.jetty.websocket.jsr356.server.JsrHandshakeRequest;
import org.eclipse.jetty.websocket.jsr356.server.JsrHandshakeResponse;
import org.eclipse.jetty.websocket.jsr356.server.PathParamServerEndpointConfig;
import org.eclipse.jetty.websocket.jsr356.server.ServerEndpointMetadata;
import org.eclipse.jetty.websocket.servlet.ServletUpgradeRequest;
import org.eclipse.jetty.websocket.servlet.ServletUpgradeResponse;
import org.eclipse.jetty.websocket.servlet.WebSocketCreator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Modified copy of the org.eclipse.jetty.websocket.jsr356.server.JsrCreator class. The original
 * implementation instantiates a new endpoint instance for each connection.
 * Instead of creating a new instance for each request the the OSGi service from the service
 * registry is re-used.
 */
public class OsgiCreator implements WebSocketCreator
{
    public static final String PROP_REMOTE_ADDRESS = "javax.websocket.endpoint.remoteAddress";
    public static final String PROP_LOCAL_ADDRESS = "javax.websocket.endpoint.localAddress";
    public static final String PROP_LOCALES = "javax.websocket.upgrade.locales";
    private static final Logger LOG = LoggerFactory.getLogger(OsgiCreator.class);
    private final WebSocketContainerScope containerScope;
    private final ServerEndpointMetadata metadata;
    private final ExtensionFactory extensionFactory;
    private final Object endpoint;

    public OsgiCreator(Object endpoint, WebSocketContainerScope containerScope, ServerEndpointMetadata metadata, ExtensionFactory extensionFactory)
    {
        // Amdatu Web - Minor difference here we take the endpoint instance as an additional constructor argument
        this.endpoint = endpoint;
		this.containerScope = containerScope;
        this.metadata = metadata;
        this.extensionFactory = extensionFactory;
    }

    @Override
    public Object createWebSocket(ServletUpgradeRequest req, ServletUpgradeResponse resp)
    {
        JsrHandshakeRequest jsrHandshakeRequest = new JsrHandshakeRequest(req);
        JsrHandshakeResponse jsrHandshakeResponse = new JsrHandshakeResponse(resp);

        // Get raw config, as defined when the endpoint was added to the container
        ServerEndpointConfig config = metadata.getConfig();

        // Establish a copy of the config, so that the UserProperties are unique
        // per upgrade request.
        config = new BasicServerEndpointConfig(containerScope, config);

        // Bug 444617 - Expose localAddress and remoteAddress for jsr modify handshake to use
        // This is being implemented as an optional set of userProperties so that
        // it is not JSR api breaking.  A few users on #jetty and a few from cometd
        // have asked for access to this information.
        Map<String, Object> userProperties = config.getUserProperties();
        userProperties.put(PROP_LOCAL_ADDRESS,req.getLocalSocketAddress());
        userProperties.put(PROP_REMOTE_ADDRESS,req.getRemoteSocketAddress());
        userProperties.put(PROP_LOCALES,Collections.list(req.getLocales()));

        // Get Configurator from config object (not guaranteed to be unique per endpoint upgrade)
        ServerEndpointConfig.Configurator configurator = config.getConfigurator();

        // [JSR] Step 1: check origin
        if (!configurator.checkOrigin(req.getOrigin()))
        {
            try
            {
                resp.sendForbidden("Origin mismatch");
            }
            catch (IOException e)
            {
                if (LOG.isDebugEnabled())
                    LOG.debug("Unable to send error response",e);
            }
            return null;
        }

        // [JSR] Step 2: deal with sub protocols
        List<String> supported = config.getSubprotocols();
        List<String> requested = req.getSubProtocols();
        String subprotocol = configurator.getNegotiatedSubprotocol(supported,requested);
        if (StringUtil.isNotBlank(subprotocol))
        {
            resp.setAcceptedSubProtocol(subprotocol);
        }

        // [JSR] Step 3: deal with extensions
        List<Extension> installedExtensions = new ArrayList<>();
        for (String extName : extensionFactory.getAvailableExtensions().keySet())
        {
            installedExtensions.add(new JsrExtension(extName));
        }
        List<Extension> requestedExts = new ArrayList<>();
        for (ExtensionConfig reqCfg : req.getExtensions())
        {
            requestedExts.add(new JsrExtension(reqCfg));
        }
        List<Extension> usedExtensions = configurator.getNegotiatedExtensions(installedExtensions,requestedExts);
        List<ExtensionConfig> configs = new ArrayList<>();
        if (usedExtensions != null)
        {
            for (Extension used : usedExtensions)
            {
                ExtensionConfig ecfg = new ExtensionConfig(used.getName());
                for (Parameter param : used.getParameters())
                {
                    ecfg.setParameter(param.getName(),param.getValue());
                }
                configs.add(ecfg);
            }
        }
        resp.setExtensions(configs);

        // [JSR] Step 4: build out new ServerEndpointConfig
        PathSpec pathSpec = jsrHandshakeRequest.getRequestPathSpec();
        if (pathSpec instanceof UriTemplatePathSpec)
        {
            // We have a PathParam path spec
            UriTemplatePathSpec wspathSpec = (UriTemplatePathSpec)pathSpec;
            String requestPath = req.getRequestPath();
            // Wrap the config with the path spec information
            config = new PathParamServerEndpointConfig(containerScope,config,wspathSpec,requestPath);
        }

        // [JSR] Step 5: Call modifyHandshake
        configurator.modifyHandshake(config,jsrHandshakeRequest,jsrHandshakeResponse);

/*
 * Amdatu Web - Instead of creating a new instance we return the instance we have
 */
//        try
//        {
//            // [JSR] Step 6: create endpoint class
//            Class<?> endpointClass = config.getEndpointClass();
//            Object endpoint = config.getConfigurator().getEndpointInstance(endpointClass);
//            // Do not decorate here (let the Connection and Session start first)
//            // This will allow CDI to see Session for injection into Endpoint classes.
//        }
//        catch (InstantiationException e)
//        {
//            if (LOG.isDebugEnabled())
//                LOG.debug("Unable to create websocket: " + config.getEndpointClass().getName(),e);
//            return null;
//        }

        return new EndpointInstance(endpoint,config,metadata);
/*
 * END
 */
    }

    @Override
    public String toString()
    {
        return String.format("%s[metadata=%s]",this.getClass().getName(),metadata);
    }
}
