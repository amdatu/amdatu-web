/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.websocket.impl;

import java.io.IOException;
import java.security.Principal;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.DeploymentException;
import javax.websocket.Session;

import org.eclipse.jetty.http.pathmap.MappedResource;
import org.eclipse.jetty.http.pathmap.PathSpec;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.websocket.jsr356.server.ServerContainer;
import org.eclipse.jetty.websocket.jsr356.server.ServerEndpointMetadata;
import org.eclipse.jetty.websocket.server.NativeWebSocketConfiguration;
import org.eclipse.jetty.websocket.server.WebSocketServerFactory;
import org.eclipse.jetty.websocket.servlet.WebSocketCreator;
import org.eclipse.jetty.websocket.servlet.WebSocketServlet;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AmdatuWebsocketServlet extends WebSocketServlet {

    private final class BasicPrincipal implements Principal {

        private final String m_name;

        private BasicPrincipal(String name) {
            m_name = name;
        }

        @Override
        public String getName() {
            return m_name;
        }

    }

    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LoggerFactory.getLogger(AmdatuWebsocketServlet.class);

    private final Set<Object> endpoints = new HashSet<>();

    private WebSocketServletFactory webSocketServletFactory;
    private NativeWebSocketConfiguration configuration;
    private ServerContainer serverContainer;



    void addEndpoint(Object service) {
        LOGGER.info("Web socket endpoint added");
        synchronized (endpoints) {
            endpoints.add(service);
            if (serverContainer != null && serverContainer.isRunning()) {
                registerEndpoint(service);
            }
        }
    }

    private void registerEndpoint(Object service) {
        try {
            ServerEndpointMetadata metadata = serverContainer.getServerEndpointMetadata(service.getClass(), null);
            OsgiCreator creator = new OsgiCreator(service, serverContainer, metadata,
                configuration.getFactory().getExtensionFactory());
            this.configuration.addMapping("uri-template|" + metadata.getPath(), creator);

            LOGGER.info(
                "Web socket endpoint '" + service.getClass() + "' registered at path: '" + metadata.getPath() + "'");
        }
        catch (DeploymentException e) {
            LOGGER.error("Failed to register web socket endpoint '" + service.getClass() + "'", e);
        }
    }

    void removeEndpoint(Object service) {
        ServerEndpointMetadata metadata;
        try {
            if (serverContainer != null && serverContainer.isRunning()) {
                metadata = serverContainer.getServerEndpointMetadata(service.getClass(), null);
                this.configuration.removeMapping("uri-template|" + metadata.getPath());

                LOGGER.info(
                    "Web socket endpoint '" + service.getClass() + "' removed from path: '" + metadata.getPath() + "'");
            }
            else {
                // The container has already stopped no need to do anything
            }
        }
        catch (DeploymentException e) {
            LOGGER.error("Failed to remove web socket endpoint '" + service.getClass() + "'", e);
        }
    }

    // dm callback
    protected final void dmStop() {
        if (serverContainer != null && serverContainer.isRunning()) {
            try {
                serverContainer.stop();
            }
            catch (Exception e) {
                LOGGER.error("Failed to stop web socket server container.]", e);
            }
        }
    }

    @Override
    public void init() throws ServletException {
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(ServerContainer.class.getClassLoader());
            super.init();
        }
        finally {
            Thread.currentThread().setContextClassLoader(contextClassLoader);
        }
    }

    /**
     * Override {@link WebSocketServlet#getServletContext()} to obtain the servlet context created by jetty without
     * the wrappers that the Apache Felix Http Jetty implementatation creates around them.
     *
     * Not sure whether it's intentional but {@link ServletContext#getContext(String)} seems to work for this as that
     * doesn't wrap the context.
     */
    @Override
    public ServletContext getServletContext() {
        ServletContext servletContext = super.getServletContext();
        return servletContext.getContext(servletContext.getContextPath());
    }

    @Override
    public void configure(WebSocketServletFactory factory) {
        webSocketServletFactory = factory;
        configuration = new NativeWebSocketConfiguration((WebSocketServerFactory) factory);

        // Create the Jetty ServerContainer implementation
        ServletContextHandler context =
            (ServletContextHandler) ServletContextHandler.getContextHandler(getServletContext());
        serverContainer = new ServerContainer(configuration, context.getServer().getThreadPool());

        try {
            synchronized (endpoints) {
                serverContainer.start();
                for (Object endpoint : endpoints) {
                    registerEndpoint(endpoint);
                }
            }
        }
        catch (Exception e) {
            LOGGER.error("Failed to confifure web socket servlet", e);
        }
    }

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

        HttpServletRequestWrapper wrappedRequest = new HttpServletRequestWrapper(request) {
            /**
             * The WebSocket spec expects Endpoints to be registered at the root of a context.
             * In this implementation the the endpoints are registered with a servlet. Because of this the servlet
             * path needs to be removed from ther request uri.
             *
             * If we don't remove the servlet path from the request uri reading the path params fails.
             */
            @Override
            public String getRequestURI() {
                String contextPath = super.getContextPath();
                String target = super.getRequestURI();
                String servletPath = super.getServletPath();

                if (target.startsWith(contextPath)) {
                    target = target.substring(contextPath.length());
                }
                if (target.startsWith(servletPath)) {
                    target = target.substring(servletPath.length());
                }

                return target;
            }

            /**
             * Return a principal containing the remote user, the http request won't be available
             * from the web socket session this makes that the remote user is accessible using the
             * {@link Session#getUserPrincipal()} method.
             */
            @Override
            public Principal getUserPrincipal() {

                String remoteUser = super.getRemoteUser();

                if (remoteUser == null) {
                    return null;
                }

                return new BasicPrincipal(remoteUser);
            }
        };

        if (webSocketServletFactory.isUpgradeRequest(wrappedRequest, response)) {
            String target = wrappedRequest.getRequestURI();
            MappedResource<WebSocketCreator> resource = configuration.getMatch(target);
            if (resource != null) {
                WebSocketCreator creator = resource.getResource();

                // Store PathSpec resource mapping as request attribute
                wrappedRequest.setAttribute(PathSpec.class.getName(), resource.getPathSpec());

                if (webSocketServletFactory.acceptWebSocket(creator, wrappedRequest, response)) {
                    // We have a socket instance created
                    return;
                }
            }
            if (response.isCommitted()) {
                // not much we can do at this point.
                return;
            }
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }


        if (response.isCommitted()) {
            // not much we can do at this point.
            return;
        }

        response.sendError(HttpServletResponse.SC_BAD_REQUEST);
    }

}
