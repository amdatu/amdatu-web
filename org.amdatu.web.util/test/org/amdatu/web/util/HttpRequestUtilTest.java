/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.util;

import static org.amdatu.web.util.HttpHeaders.ACCEPT;
import static org.amdatu.web.util.HttpHeaders.FORWARDED;
import static org.amdatu.web.util.HttpHeaders.X_FORWARDED_PROTO;
import static org.amdatu.web.util.HttpRequestUtil.accepts;
import static org.amdatu.web.util.HttpRequestUtil.isSecureRequest;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class HttpRequestUtilTest {

    @Test
    public void testHttpsUrlIsSecureOk() throws Exception {
        HttpServletRequest req = mock(HttpServletRequest.class);
        when(req.getRequestURL()).thenReturn(new StringBuffer("http://localhost/path/to/resource")); // !!!
        when(req.isSecure()).thenReturn(Boolean.TRUE);

        assertTrue(isSecureRequest(req));
    }

    @Test
    public void testHttpUrlIsNotSecureOk() throws Exception {
        HttpServletRequest req = mock(HttpServletRequest.class);
        when(req.getRequestURL()).thenReturn(new StringBuffer("http://localhost/path/to/resource"));
        when(req.isSecure()).thenReturn(Boolean.FALSE);

        assertFalse(isSecureRequest(req));
    }

    @Test
    public void testUseOffloadedHttpRequestsForwardedOk() throws Exception {
        HttpServletRequest req = mock(HttpServletRequest.class);
        when(req.getRequestURL()).thenReturn(new StringBuffer("http://localhost/path/to/resource"));
        when(req.getHeader(eq(FORWARDED))).thenReturn("for=1.2.3.4;proto=http");

        assertFalse(isSecureRequest(req));
    }

    @Test
    public void testUseOffloadedHttpRequestsXForwardedProtoOk() throws Exception {
        HttpServletRequest req = mock(HttpServletRequest.class);
        when(req.getRequestURL()).thenReturn(new StringBuffer("http://localhost/path/to/resource"));
        when(req.getHeader(eq(X_FORWARDED_PROTO))).thenReturn("HTTP");

        assertFalse(isSecureRequest(req));
    }

    @Test
    public void testUseOffloadedHttpsRequestsForwardedOk() throws Exception {
        HttpServletRequest req = mock(HttpServletRequest.class);
        when(req.getRequestURL()).thenReturn(new StringBuffer("http://localhost/path/to/resource"));
        when(req.getHeader(eq(FORWARDED))).thenReturn("for=1.2.3.4;proto=https");

        assertTrue(isSecureRequest(req));
    }

    @Test
    public void testUseOffloadedHttpsRequestsXForwardedProtoOk() throws Exception {
        HttpServletRequest req = mock(HttpServletRequest.class);
        when(req.getRequestURL()).thenReturn(new StringBuffer("http://localhost/path/to/resource"));
        when(req.getHeader(eq(X_FORWARDED_PROTO))).thenReturn("HTTPS");

        assertTrue(isSecureRequest(req));
    }

    @Test
    public void testAcceptsWithoutGivenAcceptsHeaderOk() throws Exception {
        HttpServletRequest req = mock(HttpServletRequest.class);
        when(req.getHeader(eq(ACCEPT))).thenReturn(null);

        assertTrue(accepts(req, "text/plain"));
        assertTrue(accepts(req, "text/html"));
        assertTrue(accepts(req, "application/json"));
        assertTrue(accepts(req, "application/json;type=foo"));
    }

    @Test
    public void testAcceptsWithGivenAcceptsHeaderOk() throws Exception {
        HttpServletRequest req = mock(HttpServletRequest.class);
        when(req.getHeader(eq(ACCEPT))).thenReturn("text/*, text/html, text/html;level=1");

        assertTrue(accepts(req, "text/plain"));
        assertTrue(accepts(req, "text/html"));
        assertTrue(accepts(req, "text/html;level=1"));
        assertFalse(accepts(req, "application/json"));
        assertFalse(accepts(req, "application/json;type=foo"));

        when(req.getHeader(eq(ACCEPT))).thenReturn("application/json;type=foo");

        assertTrue(accepts(req, "application/json"));
        assertTrue(accepts(req, "application/json;type=foo"));
        assertTrue(accepts(req, "application/json;type=foo;q=1"));
        assertFalse(accepts(req, "text/plain"));
        assertFalse(accepts(req, "text/html"));
        assertFalse(accepts(req, "text/html;level=1"));
        assertFalse(accepts(req, "application/json;type=bar"));

        when(req.getHeader(eq(ACCEPT))).thenReturn("text/html;q=0.7, text/html;level=1;q=0.3, text/html;level=2;Q=0.4");

        assertTrue(accepts(req, "text/html"));
        assertTrue(accepts(req, "text/html;level=1"));
        assertTrue(accepts(req, "text/html;level=2"));
        assertFalse(accepts(req, "text/html;level=3"));
        assertFalse(accepts(req, "text/plain"));
        assertFalse(accepts(req, "application/json;type=bar"));
        assertFalse(accepts(req, "application/json"));
        assertFalse(accepts(req, "application/json;type=foo"));
        assertFalse(accepts(req, "application/json;type=foo;q=1"));
    }
}
