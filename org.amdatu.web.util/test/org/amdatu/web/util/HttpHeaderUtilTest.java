/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.util;

import static java.nio.charset.StandardCharsets.ISO_8859_1;
import static java.nio.charset.StandardCharsets.US_ASCII;
import static java.nio.charset.StandardCharsets.*;
import static org.amdatu.web.util.HttpHeaderUtil.encodeParameterField;
import static org.amdatu.web.util.HttpHeaderUtil.*;
import static org.junit.Assert.assertEquals;

import java.nio.charset.Charset;

import org.junit.Test;

public class HttpHeaderUtilTest {

    @Test
    public void testContentDispositionHeaderOk() {
        String hdr = getContentDispositionHeader("qux.pdf", false);
        assertEquals("inline; filename=\"qux.pdf\"", hdr);
    }

    @Test
    public void testContentDispositionHeaderWithQuotedStringOk() {
        String hdr;

        hdr = getContentDispositionHeader("qux \"quu\".pdf", false);
        assertEquals("inline; filename=\"qux quu.pdf\"", hdr);

        hdr = getContentDispositionHeader("qux\u20ac \"quu\".pdf", false);
        assertEquals("inline; filename=\"qux quu.pdf\"; filename*=UTF-8''qux%E2%82%AC%20quu.pdf", hdr);
    }

    @Test
    public void testContentDispositionHeaderWithUTF8Ok() {
        String hdr = getContentDispositionHeader("qux\u20ac.quu", false);
        assertEquals("inline; filename=\"qux.quu\"; filename*=UTF-8''qux%E2%82%AC.quu", hdr);
    }

    @Test
    public void testEncodedContentDispositionHeaderOk() throws Exception {
        String name = new String(new byte[] { 'b', (byte) 0xb5, 'r', '.', 't', 'x', 't' }, ISO_8859_1);
        String hdr = getContentDispositionHeader(name, true);
        assertEquals("attachment; filename=\"bµr.txt\"", hdr);
    }

    @Test
    public void testEncodePlainISO8859_1Value() throws Exception {
        assertEquals("foo=\"myValue\"", encodeParameterField("foo", "myValue"));
    }

    @Test
    public void testEncodeValueWithASCIIRepresentation() throws Exception {
        assertEquals("currency=\"Euro\"", encodeParameterField("currency", "Euro", US_ASCII));
    }

    @Test
    public void testEncodeValueWithUTF16Representation() throws Exception {
        assertEquals("currency*=UTF-16''%FE%FFgqN%AC", encodeParameterField("currency", "\u6771\u4eac", UTF_16));
    }

    @Test
    public void testEncodeValueWithCp1252Representation() throws Exception {
        assertEquals("currency*=windows-1252''%80", encodeParameterField("currency", "\u20ac", Charset.forName("Cp1252")));
    }

    @Test
    public void testEncodeValueWithISO8859_1Representation() throws Exception {
        assertEquals("currency=\"\u00a4\"", encodeParameterField("currency", "\u00a4", ISO_8859_1));
    }

    @Test
    public void testEncodeValueWithUTF8Representation() throws Exception {
        assertEquals("currency*=UTF-8''%E2%82%AC", encodeParameterField("currency", "\u20ac", UTF_8));
    }

    @Test
    public void testRemoveControlCharactersOk() {
        StringBuilder in = new StringBuilder("a\\/:*?\"<>|");
        for (int i = 0x00; i <= 0x1f; i++) {
            in.append((char) i);
        }
        in.append('b');

        assertEquals("ab", removeControlCharacters(in.toString()));
    }

}
