/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.util.upload;

import static java.util.Optional.empty;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_CONFLICT;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_NO_CONTENT;
import static javax.servlet.http.HttpServletResponse.SC_REQUEST_ENTITY_TOO_LARGE;
import static org.amdatu.web.util.HttpMockUtils.asString;
import static org.amdatu.web.util.HttpMockUtils.mockHttpFilePostRequest;
import static org.amdatu.web.util.HttpMockUtils.mockHttpFormPostRequest;
import static org.amdatu.web.util.HttpMockUtils.mockHttpResponse;
import static org.amdatu.web.util.HttpMockUtils.toMap;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;

/**
 * Test cases for {@link UploadHelper}.
 */
public class UploadHelperTest {

    private UploadHelper<TestResource> m_helper;

    @Test
    public void testContentsOfUploadedFileCanBeReadMultipleTimes() throws Exception {
        m_helper = new UploadHelper<>(new UploadHandler<TestResource>() {
            @Override
            public Optional<TestResource> addResource(UploadedFile file) throws UploadException {
                try {
                    for (int i = 0; i < 2; i++) {
                        assertEquals("hello world!", asString(file.getContent()));
                    }
                    return empty();
                }
                catch (IOException e) {
                    throw new UploadException("shouldn't happen!", e);
                }
            }
        }, 1024 /* b */);

        HttpServletRequest req = mockHttpFilePostRequest("test.txt");
        HttpServletResponse resp = mockHttpResponse();

        Optional<TestResource> result = m_helper.handleSingleUpload(req, resp);
        assertFalse(result.isPresent());
    }

    @Test
    public void testPostForm() throws Exception {
        m_helper = createUploadHelper();

        HttpServletRequest req = mockHttpFormPostRequest(toMap("key", "value"));
        HttpServletResponse resp = mockHttpResponse();

        assertEmpty(m_helper.handleUpload(req, resp));

        verify(resp).setStatus(eq(SC_NO_CONTENT));
        verifyNoMoreInteractions(resp);
    }

    @Test
    public void testRejectUploadsThatAreTooBig() throws Exception {
        m_helper = new UploadHelper<>(new TestUploadHandler(), 4 /* b */);

        HttpServletRequest req = mockHttpFilePostRequest("test.txt");
        HttpServletResponse resp = mockHttpResponse();

        assertEmpty(m_helper.handleUpload(req, resp));

        verify(resp).setStatus(eq(SC_REQUEST_ENTITY_TOO_LARGE));
        verify(resp).flushBuffer();
        verifyNoMoreInteractions(resp);
    }

    @Test
    public void testUploadEmptyBody() throws Exception {
        m_helper = createUploadHelper();

        HttpServletRequest req = mockHttpFilePostRequest(new String[0]);
        HttpServletResponse resp = mockHttpResponse();

        assertEmpty(m_helper.handleUpload(req, resp));

        verify(resp).setStatus(eq(SC_NO_CONTENT));
        verifyNoMoreInteractions(resp);
    }

    @Test
    public void testUploadFileWithMangledData() throws Exception {
        m_helper = createUploadHelper();

        HttpServletRequest req = mockHttpFilePostRequest("mangled_body");
        HttpServletResponse resp = mockHttpResponse();

        assertEmpty(m_helper.handleUpload(req, resp));

        verify(resp).setStatus(eq(SC_BAD_REQUEST));
        verify(resp).flushBuffer();
        verifyNoMoreInteractions(resp);
    }

    @Test
    public void testUploadFileWithUploadException() throws Exception {
        m_helper = createUploadHelper();

        HttpServletRequest req = mockHttpFilePostRequest("exception");
        HttpServletResponse resp = mockHttpResponse();

        assertEmpty(m_helper.handleUpload(req, resp));

        verify(resp).setStatus(eq(SC_CONFLICT));
        verify(resp).flushBuffer();
        verifyNoMoreInteractions(resp);
    }

    @Test
    public void testUploadMultipleFiles() throws Exception {
        m_helper = createUploadHelper();

        HttpServletRequest req = mockHttpFilePostRequest(new String[] { "test1.txt", "test2.txt" });
        HttpServletResponse resp = mockHttpResponse();

        Optional<List<TestResource>> result = m_helper.handleUpload(req, resp);
        assertTrue(result.isPresent());

        List<TestResource> resources = result.get();
        assertEquals(2, resources.size());
    }

    @Test
    public void testUploadSingleFile() throws Exception {
        m_helper = createUploadHelper();

        HttpServletRequest req = mockHttpFilePostRequest("test.txt");
        HttpServletResponse resp = mockHttpResponse();

        Optional<TestResource> result = m_helper.handleSingleUpload(req, resp);
        assertTrue(result.isPresent());

        TestResource resource = result.get();
        assertNotNull(resource);
    }

    @Test
    public void testUploadWrongContentType() throws Exception {
        m_helper = createUploadHelper();

        HttpServletRequest req = mockHttpFilePostRequest(new String[0]);
        when(req.getContentType()).thenReturn("text/plain");

        HttpServletResponse resp = mockHttpResponse();

        assertEmpty(m_helper.handleUpload(req, resp));

        verify(resp).setStatus(eq(SC_BAD_REQUEST));
        verify(resp).flushBuffer();
        verifyNoMoreInteractions(resp);
    }

    @Test
    public void testUploadFileWithIOException() throws Exception {
        m_helper = createUploadHelper();

        HttpServletRequest req = mockHttpFilePostRequest("ioexception");
        HttpServletResponse resp = mockHttpResponse();

        Optional<TestResource> result = m_helper.handleSingleUpload(req, resp);
        assertFalse(result.isPresent());

        verify(resp).setStatus(eq(SC_INTERNAL_SERVER_ERROR));
        verify(resp).flushBuffer();
        verifyNoMoreInteractions(resp);
    }

    @Test
    public void testUploadWrongMethod() throws Exception {
        m_helper = createUploadHelper();

        HttpServletRequest req = mockHttpFilePostRequest(new String[0]);
        when(req.getMethod()).thenReturn("PUT");

        HttpServletResponse resp = mockHttpResponse();

        assertEmpty(m_helper.handleUpload(req, resp));

        verify(resp).setStatus(eq(SC_BAD_REQUEST));
        verify(resp).flushBuffer();
        verifyNoMoreInteractions(resp);
    }

    private void assertEmpty(Optional<List<TestResource>> handle) {
        assertFalse(handle.isPresent());
    }

    private UploadHelper<TestResource> createUploadHelper() {
        return new UploadHelper<>(new TestUploadHandler(), 1024 /* b */);
    }

}
