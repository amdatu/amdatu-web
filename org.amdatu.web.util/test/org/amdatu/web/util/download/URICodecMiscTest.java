/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.util.download;

import static java.nio.charset.StandardCharsets.*;
import static org.amdatu.web.util.URICodec.getDecoder;
import static org.amdatu.web.util.URICodec.getEncoder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.amdatu.web.util.URICodec;
import org.amdatu.web.util.URICodec.MalformedURIException;
import org.amdatu.web.util.URICodec.URICodecException;
import org.junit.Test;

/**
 * Test cases for {@link URICodec}.
 */
public class URICodecMiscTest {

    @Test(expected = MalformedURIException.class)
    public void testDecodeInvalidHexEncodingFirstNibbleFail() {
        assertNull(getDecoder().decode("a%FH"));
    }

    @Test(expected = MalformedURIException.class)
    public void testDecodeInvalidHexEncodingSecondNibbleFail() {
        assertNull(getDecoder().decode("a%HF"));
    }

    @Test(expected = MalformedURIException.class)
    public void testDecodeInvalidHexPairFail() {
        assertNull(getDecoder().decode("a%F"));
    }

    @Test(expected = MalformedURIException.class)
    public void testDecodeInvalidSurrogateFail() {
        assertNull(getDecoder().decode("a%F0%90"));
    }

    @Test(expected = MalformedURIException.class)
    public void testDecodeMalformedInputFail() {
        assertNull(getDecoder().decode("a%E2%82%AC", US_ASCII));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDecodeNullInputFail() {
        assertNull(getDecoder().decode(null));
    }

    @Test
    public void testEncodeDecodeWorksForJavaScriptOk() throws Exception {
        int charCount = 1024;

        ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
        engine.eval("i = \"\";\n" +
            "for (j = 0; j < " + charCount + "; j++) { i = i + String.fromCharCode(j); }\n" +
            "k = encodeURIComponent(i);");
        String jsEncoded = engine.get("k").toString();

        StringBuilder input = new StringBuilder(1024);
        for (int i = 0; i < 1024; i++) {
            input.append((char) i);
        }

        assertEquals(jsEncoded, getEncoder().encode(input.toString()));
        assertEquals(input.toString(), getDecoder().decode(jsEncoded));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEncodeNullInputFail() {
        assertNull(getEncoder().encode(null));
    }

    @Test(expected = URICodecException.class)
    public void testEncodeUnmappableCharacterFail() {
        assertNull(getEncoder().encode("\uD801\uDC37", US_ASCII));
    }
}
