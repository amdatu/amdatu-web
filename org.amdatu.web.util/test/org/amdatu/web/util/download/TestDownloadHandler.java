/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.util.download;

import static java.util.Objects.requireNonNull;

import java.time.Instant;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

class TestDownloadHandler implements DownloadHandler {
    private TestResource m_resource;

    private TestDownloadHandler() {
        m_resource = null;
    }

    private TestDownloadHandler(TestResource resource) {
        m_resource = requireNonNull(resource);
    }

    public static TestDownloadHandler createResourceProvider() {
        return new TestDownloadHandler();
    }

    public static TestDownloadHandler createResourceProvider(String name, int length) {
        return new TestDownloadHandler(new TestResource(name, length, Instant.now().minusSeconds(1)));
    }

    public static TestDownloadHandler createResourceProvider(String name, int length, Instant lastModified) {
        return new TestDownloadHandler(new TestResource(name, length, lastModified));
    }

    public static TestDownloadHandler createResourceProvider(String name, int length, String eTag,
        Instant lastModified) {
        return new TestDownloadHandler(new TestResource(name, length, eTag, lastModified));
    }

    public static TestDownloadHandler createResourceProvider(TestResource resource) {
        return new TestDownloadHandler(resource);
    }

    public TestResource getResource() {
        return m_resource;
    }

    @Override
    public Optional<DownloadResource> getResource(HttpServletRequest request) {
        String pathInfo = request.getPathInfo();

        if (m_resource != null && pathInfo.equals(m_resource.getName())) {
            return Optional.of(m_resource);
        }

        return Optional.empty();
    }
}