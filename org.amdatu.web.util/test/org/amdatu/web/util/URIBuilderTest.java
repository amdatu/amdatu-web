/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.util;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;

/**
 * Test cases for {@link URIBuilder}.
 */
public class URIBuilderTest {

    private static <T> void assertOptionalValue(T expected, Optional<T> actual) {
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    private static void assertPathEquals(String input, String expectedOutput) {
        assertEquals(expectedOutput, new URIBuilder(input).build().getPath());
    }

    private static <T> void assertStream(List<T> expected, Stream<T> actual) {
        List<T> actualValues = actual.collect(toList());
        assertEquals(expected, actualValues);
    }

    private static void assertURI(String expected, URI input) {
        assertEquals(expected, input.toASCIIString());
    }

    @Test
    public void testAppendPathEncodesDirectlyOk() throws Exception {
        assertOptionalValue("foo%25qux%E2%82%ACbar", new URIBuilder()
            .appendToPath("foo%qux€bar")
            .getPath());

        assertOptionalValue("quu/foo%25qux%E2%82%ACbar", new URIBuilder("quu")
            .appendToPath("foo%qux€bar")
            .getPath());

        assertURI("foo%25qux%E2%82%ACbar", new URIBuilder()
            .appendToPath("foo%qux€bar")
            .build());
    }

    @Test
    public void testAppendQueryEncodesDirectlyOk() throws Exception {
        assertOptionalValue("foo%25qux%E2%82%ACbar", new URIBuilder()
            .appendToQuery("msg", "foo%qux€bar")
            .getQueryValue("msg"));

        assertURI("?msg=foo%25qux%E2%82%ACbar", new URIBuilder()
            .appendToQuery("msg", "foo%qux€bar")
            .build());
    }

    @Test
    public void testAppendQueryParamsToExistingParamsOk() throws Exception {
        URI input = URI.create("http://localhost/path?e&a=b");

        assertURI("http://localhost/path?e&e=g&e=h&a=b&a=f&c=d&i",
            new URIBuilder(input)
                .appendToQuery("c", "d")
                .appendToQuery("a", "f")
                .appendToQuery("e", "g", "h")
                .appendToQuery("i")
                .build());
    }

    @Test
    public void testAppendToExistingUriOk() throws Exception {
        URI input = URI.create("http://localhost/path?e&a=b#foo");
        assertURI("http://localhost/path/to/resource?e&a=b&c=d#foo",
            new URIBuilder(input).appendToQuery("c", "d").appendToPath("sub/../to/resource").build());
    }

    @Test
    public void testBuildRelativeUriOk() throws Exception {
        assertURI("local:data?size=16",
            new URIBuilder().setScheme("local").setPath("data").appendToQuery("size", "16").build());
    }

    @Test
    public void testBuildSimpleUriOk() throws Exception {
        assertURI("http://localhost/", new URIBuilder().setScheme("http").setHost("localhost").setPath("/").build());
    }

    @Test
    public void testBuildURIWithPathQueryAndFragmentOk() throws Exception {
        assertURI("http://localhost/path?a=b&c=d%20e%20f#foo", new URIBuilder("http://localhost")
            .appendToQuery("a", "b")
            .appendToPath("path")
            .appendToQuery("c", "d e f")
            .setFragment("foo")
            .build());
    }

    @Test
    public void testEmbeddedUrlInQueryOk() throws Exception {
        String url = "http://example.com/path?hl=auto&langpair=auto%7Czh-TW" +
            "&u=http%3A%2F%2Flocalhost.localnet%2F%3Fu%3Dcbc96c3d7a%26id%3D17fdad33a4%26e%3D";
        assertURI(url, new URIBuilder(url).build());
    }

    @Test
    public void testEmptyPathIsDefaultOk() throws Exception {
        assertFalse(new URIBuilder()
            .getPath().isPresent());
    }

    @Test
    public void testEscapeQueryParamsOk() throws Exception {
        URIBuilder builder = new URIBuilder();

        builder.clearQuery().appendToQuery("msg", "foo bar qux");

        assertURI("?msg=foo%20bar%20qux", builder.build());

        builder.clearQuery().appendToQuery("emoji", "\ue056", "\ue40f");

        assertURI("?emoji=%EE%81%96&emoji=%EE%90%8F", builder.build());
    }

    @Test
    public void testEscapeReservedCharactersInPathOk() throws Exception {
        URIBuilder builder = new URIBuilder();

        builder.setPath(";/?:@=&");

        assertOptionalValue("%3B/%3F%3A%40%3D%26", builder.getPath());

        builder.setPath("\"<>#%{}|\\^~[]`");

        assertOptionalValue("%22%3C%3E%23%25%7B%7D%7C%5C%5E~%5B%5D%60", builder.getPath());

        builder.setPath("\u0000\u001f\u007f");

        assertOptionalValue("%00%1F%7F", builder.getPath());
    }

    @Test
    public void testGetQueryValueOk() throws Exception {
        HttpServletRequest req = mock(HttpServletRequest.class);
        when(req.getRequestURL()).thenReturn(new StringBuffer("http://localhost"));
        when(req.getQueryString()).thenReturn("a=b&b=c&b=d&c=e&f");

        assertOptionalValue("b", URIBuilder.of(req).getQueryValue("a"));
        assertOptionalValue("c", URIBuilder.of(req).getQueryValue("b"));
        assertOptionalValue("", URIBuilder.of(req).getQueryValue("f"));
    }

    @Test
    public void testGetQueryValuesOk() throws Exception {
        HttpServletRequest req = mock(HttpServletRequest.class);
        when(req.getRequestURL()).thenReturn(new StringBuffer("http://localhost"));
        when(req.getQueryString()).thenReturn("a=b&b=c&b=d&c=e&f");

        assertStream(asList("b"), URIBuilder.of(req).getQueryValues("a"));
        assertStream(asList("c", "d"), URIBuilder.of(req).getQueryValues("b"));
        assertStream(asList(""), URIBuilder.of(req).getQueryValues("f"));
    }

    @Test
    public void testIncludeQueryStringFromRequestOk() throws Exception {
        HttpServletRequest req = mock(HttpServletRequest.class);
        when(req.getRequestURL()).thenReturn(new StringBuffer("http://localhost/path/to/resource"));
        when(req.getQueryString()).thenReturn("a=b");

        assertURI("http://localhost/path/to/resource?a=b&c=d", URIBuilder.of(req).appendToQuery("c", "d").build());
    }

    @Test
    public void testNormalizeEscapesInPathOk() throws Exception {
        URIBuilder builder = new URIBuilder();

        builder.setPath("A-Z/a-z/0-9/-/./_/~");

        assertOptionalValue("A-Z/a-z/0-9/-/./_/~", builder.getPath());

        builder.setPath("%41-%5A/%61-%7A/%30-%39/%2D/%2E/%5F/%7E");

        assertOptionalValue("A-Z/a-z/0-9/-/./_/~", builder.getPath());

        builder.setEncodedPath("%41-%5A/foo%25qux%E2%82%ACbar");

        assertOptionalValue("A-Z/foo%25qux%E2%82%ACbar", builder.getPath());

        builder.setPath("%41-%5A").appendToPath("%61-%7A/%30-%39/%2D/%2E/%5F").appendToPath("/%7E");

        assertOptionalValue("A-Z/a-z/0-9/-/./_/~", builder.getPath());
    }

    @Test
    public void testParseInvalidEscapesInPathOk() throws Exception {
        // The following escape sequences are taken from <http://www.cl.cam.ac.uk/~mgk25/ucs/examples/UTF-8-test.txt>
        // and all should result in invalid Unicode characters.
        assertPathEquals("%fe", "\uFFFD");
        assertPathEquals("%ff", "\uFFFD");
        assertPathEquals("%ed%a0%80", "\uFFFD");
        assertPathEquals("%ed%ad%bf", "\uFFFD");

        assertPathEquals("%c0%af", "\uFFFD\uFFFD");
        assertPathEquals("%c1%bf", "\uFFFD\uFFFD");

        assertPathEquals("%e0%80%af", "\uFFFD\uFFFD\uFFFD");

        assertPathEquals("%fe%fe%ff%ff", "\uFFFD\uFFFD\uFFFD\uFFFD");

        assertPathEquals("%f8%80%80%80%80", "\uFFFD\uFFFD\uFFFD\uFFFD\uFFFD");
    }

    @Test
    public void testParseInvalidUnicodeInPathOk() throws Exception {
        // Single UTF-16 surrogates
        assertPathEquals("\uDB7F", "\uDB7F");
        // Non-character...
        assertPathEquals("\uFDD0", "\uFDD0");
    }

    @Test
    public void testReplaceTemplateParametersOk() throws Exception {
        assertEquals("", replaceTemplateParameters("", "a", "b", "c"));
        assertEquals("abc", replaceTemplateParameters("abc"));
        assertEquals("abc", replaceTemplateParameters("{}bc", "a"));
        assertEquals("abc", replaceTemplateParameters("a{}c", "b"));
        assertEquals("abc", replaceTemplateParameters("ab{}", "c"));
        assertEquals("abc", replaceTemplateParameters("{}b{}", "a", "c"));
        assertEquals("a-b-c", replaceTemplateParameters("{x}-{y}-{z}", "a", "b", "c"));
        assertEquals("{x}-{y}-{z}", replaceTemplateParameters("{x}-{y}-{z}"));
        assertEquals("1-2-3", replaceTemplateParameters("{x}-{y}-{z}", 1, 2, 3));

        assertEquals("{x", replaceTemplateParameters("{x", "a", "b", "c"));
        assertEquals("x{y", replaceTemplateParameters("x{y", "a", "b", "c"));
        assertEquals("x}", replaceTemplateParameters("x}", "a", "b", "c"));
        assertEquals("x}y", replaceTemplateParameters("x}y", "a", "b", "c"));
        assertEquals("x}{y", replaceTemplateParameters("x}{y", "a", "b", "c"));
    }

    @Test
    public void testSetEmptyPathOk() throws Exception {
        assertFalse(new URIBuilder()
            .setPath("")
            .getPath().isPresent());
    }

    @Test
    public void testSetPathEncodesDirectlyOk() throws Exception {
        assertOptionalValue("foo%25qux%E2%82%ACbar", new URIBuilder()
            .setPath("foo%qux€bar")
            .getPath());

        assertURI("foo%25qux%E2%82%ACbar", new URIBuilder()
            .setPath("foo%qux€bar")
            .build());
    }

    @Test
    public void testSetQueryEncodesDirectlyOk() throws Exception {
        assertOptionalValue("foo%25qux%E2%82%ACbar", new URIBuilder()
            .setQuery("msg=foo%qux€bar")
            .getQueryValue("msg"));

        assertURI("?msg=foo%25qux%E2%82%ACbar", new URIBuilder()
            .setQuery("msg=foo%qux€bar")
            .build());
    }

    @Test
    public void testSetRootPathOk() throws Exception {
        assertOptionalValue("/", new URIBuilder()
            .setPath("/")
            .getPath());
    }

    @Test
    public void testSubPathEndingWithOk() throws Exception {
        URI input = URI.create("http://localhost/a/b/c/d");
        assertURI("http://localhost/a/b/e",
            new URIBuilder(input).subPathEndingWith("/b").appendToPath("/e").build());
        assertURI("http://localhost/a/b/e",
            new URIBuilder(input).subPathEndingWith("/a/b").appendToPath("/e").build());
        assertURI("http://localhost/a/b/c/d/e",
            new URIBuilder(input).subPathEndingWith("/c/d").appendToPath("/e").build());
        assertURI("http://localhost/e",
            new URIBuilder(input).subPathEndingWith("/q").appendToPath("/e").build());
    }

    @Test
    public void testTemplateParametersAreEncodedOk() throws Exception {
        assertURI("http://localhost:8484/a/b/c%25d?foo=b/a/r&bar=foo#qux=q/u?u", new URIBuilder()
            .setScheme("{scheme}")
            .setHost("{host}")
            .appendToPath("{path1}")
            .appendToPath("{path2}")
            .setQuery("foo={param1}")
            .appendToQuery("bar", "{param2}")
            .setFragment("qux={param2}")
            .build("http", "localhost:8484", "a/b", "c%25d", "b/a/r", "foo", "q/u?u"));
    }

    @Test
    public void testUseOffloadedHttpsRequestsForwardedOk() throws Exception {
        HttpServletRequest req = mock(HttpServletRequest.class);
        when(req.getRequestURL()).thenReturn(new StringBuffer("http://localhost/path/to/resource"));
        when(req.getHeader(eq("Forwarded"))).thenReturn("for=1.2.3.4;proto=https");

        assertURI("https://localhost/path/to/resource?c=d", URIBuilder.of(req).appendToQuery("c", "d").build());
    }

    @Test
    public void testUseOffloadedHttpsRequestsXForwardedProtoOk() throws Exception {
        HttpServletRequest req = mock(HttpServletRequest.class);
        when(req.getRequestURL()).thenReturn(new StringBuffer("http://localhost/path/to/resource"));
        when(req.getHeader(eq("X-Forwarded-Proto"))).thenReturn("HTTPS");

        assertURI("https://localhost/path/to/resource?c=d", URIBuilder.of(req).appendToQuery("c", "d").build());
    }

    @Test
    public void testUseTemplateParametersOk() throws Exception {
        assertURI("http://localhost/a/b/c/d?whiz=bang&foo=bar#quu",
            new URIBuilder("http://localhost/?whiz=bang")
                .setPath("/a/{1}/c/{2}")
                .appendToQuery("foo", "{3}")
                .setFragment("{qux}")
                .build("b", "d", "bar", "quu"));
    }

    private String replaceTemplateParameters(String in, Object... args) {
        List<Object> argList = new ArrayList<>();
        argList.addAll(asList(args));

        return URIBuilder.replaceTemplateParameters(in, argList).toString();
    }
}
