/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.util;

import static java.nio.charset.CodingErrorAction.IGNORE;
import static java.nio.charset.StandardCharsets.ISO_8859_1;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;
import java.util.Arrays;

/**
 * Provides convenience utils for working with HTTP headers.
 */
public class HttpHeaderUtil {

    private HttpHeaderUtil() {
        // Not to be instantiated...
    }

    /**
     * Creates the content disposition value for a given filename.
     * <p>
     * This method adheres to RFC6266, in the sense that it verifies whether the name of the resource has characters
     * that do not map to any valid ISO8859-1 encoding, and if so, includes a UTF-8 representation of the file name.
     * </p>
     *
     * @param fileName the name of the file (resource) to use, cannot be <code>null</code> or empty;
     * @param <code>true</code> if the file should be an attachment, <code>false</code> if the file should be inlined.
     * @return the value for the content disposition header, never <code>null</code>.
     */
    public static String getContentDispositionHeader(String fileName, boolean attachment) {
        StringBuilder sb = new StringBuilder();
        if (attachment) {
            sb.append("attachment; ");
        } else {
            sb.append("inline; ");
        }

        sb.append(encodeParameterField("filename", removeControlCharacters(fileName)));

        return sb.toString();
    }

    protected static String removeControlCharacters(String value) {
        // Remove all characters that aren't supported on Windows platforms
        // (on OSX/Linux, the only reserved character is '/')...
        return value.replaceAll("[\\p{Cntrl}\\\\/:*?\"<>|]+", "");
    }

    /**
     * Encodes a header parameter according to RFC5987 using UTF-8 as the potential encoded representation of the
     * parameter value.
     *
     * @param name the name of the parameter, like "filename";
     * @param value the value of the parameter, like "myFile.txt".
     * @return the encoded parameter field, like "filename=myFile.txt".
     */
    public static String encodeParameterField(String name, String value) {
        return encodeParameterField(name, value, UTF_8);
    }

    /**
     * Encodes a header parameter according to RFC5987 using a given character set as the potential encoded
     * representation of the parameter value.
     *
     * @param name the name of the parameter, like "filename";
     * @param value the value of the parameter, like "myFile.txt";
     * @param charset the character set to use for the encoded representation.
     * @return the encoded parameter field, like 'filename="myFile.txt"'.
     */
    public static String encodeParameterField(String name, String value, Charset charset) {
        StringBuilder sb = new StringBuilder();

        // Check whether we've got a name that consists of valid ISO8859-1 characters...
        String iso8859_1_safe = decode(ISO_8859_1, encode(ISO_8859_1, value));

        if (!"".equals(iso8859_1_safe)) {
            sb.append(name).append("=\"").append(iso8859_1_safe).append("\"");
        }
        if (!iso8859_1_safe.equals(value)) {
            appendEncodedParameter(sb, charset, name, value);
        }
        return sb.toString();
    }

    protected static void appendEncodedParameter(StringBuilder sb, Charset charset, String name, String value) {
        String charsetName = charset.name();
        String encodedName = URICodec.getEncoder().encode(value, charset);
        if (sb.length() > 0) {
            sb.append("; ");
        }
        sb.append(name).append("*=").append(charsetName).append("''").append(encodedName);
    }

    private static int scale(int len, float expansionFactor) {
        // We need to perform double, not float, arithmetic; otherwise
        // we lose low order bits when len is larger than 2**24.
        return (int) (len * (double) expansionFactor);
    }

    private static byte[] encode(Charset cs, String ca) {
        CharsetEncoder ce = cs.newEncoder().onMalformedInput(IGNORE).onUnmappableCharacter(IGNORE);

        int en = scale(ca.length(), ce.maxBytesPerChar());
        byte[] ba = new byte[en];
        if (en == 0) {
            return ba;
        }

        ByteBuffer bb = ByteBuffer.wrap(ba);
        CharBuffer cb = CharBuffer.wrap(ca);
        try {
            CoderResult cr = ce.encode(cb, bb, true);
            if (!cr.isUnderflow()) {
                cr.throwException();
            }
            cr = ce.flush(bb);
            if (!cr.isUnderflow()) {
                cr.throwException();
            }
        } catch (CharacterCodingException x) {
            throw new RuntimeException(x);
        }

        return Arrays.copyOf(ba, bb.position());
    }

    private static String decode(Charset cs, byte[] ba) {
        CharsetDecoder cd = cs.newDecoder().onMalformedInput(IGNORE).onUnmappableCharacter(IGNORE);

        int en = scale(ba.length, cd.maxCharsPerByte());
        char[] ca = new char[en];
        if (en == 0) {
            return "";
        }

        ByteBuffer bb = ByteBuffer.wrap(ba);
        CharBuffer cb = CharBuffer.wrap(ca);
        try {
            CoderResult cr = cd.decode(bb, cb, true);
            if (!cr.isUnderflow()) {
                cr.throwException();
            }
            cr = cd.flush(cb);
            if (!cr.isUnderflow()) {
                cr.throwException();
            }
        } catch (CharacterCodingException x) {
            throw new RuntimeException(x);
        }
        return cb.flip().toString();
    }

}
