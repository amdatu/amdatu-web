/*
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.amdatu.web.util;

import static java.util.Arrays.binarySearch;
import static java.util.Arrays.sort;
import static java.util.Arrays.stream;
import static java.util.regex.Pattern.CASE_INSENSITIVE;
import static java.util.stream.Collectors.groupingBy;
import static org.amdatu.web.util.HttpHeaders.ACCEPT;
import static org.amdatu.web.util.HttpHeaders.FORWARDED;
import static org.amdatu.web.util.HttpHeaders.IF_MATCH;
import static org.amdatu.web.util.HttpHeaders.IF_MODIFIED_SINCE;
import static org.amdatu.web.util.HttpHeaders.IF_NONE_MATCH;
import static org.amdatu.web.util.HttpHeaders.IF_UNMODIFIED_SINCE;
import static org.amdatu.web.util.HttpHeaders.X_FORWARDED_PROTO;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalLong;
import java.util.regex.Pattern;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * Convenience methods for working with {@link HttpServletRequest}s.
 */
public class HttpRequestUtil {
    private static final String HTTPS = "https";

    private static final Pattern PROTO_HTTPS_MATCHES = Pattern.compile("\\bproto=https\\b", CASE_INSENSITIVE);
    private static final Pattern WEIGHT_PATTERN = Pattern.compile(";q=[01](\\.[0-9]{0,3})?", CASE_INSENSITIVE);

    /**
     * Returns <code>true</code> if the given request contains an accept header that accepts the given content type,
     * <code>false</code> otherwise.
     */
    public static boolean accepts(HttpServletRequest req, String contentType) {
        String acceptHeader = req.getHeader(ACCEPT);
        if (acceptHeader == null) {
            return true;
        }
        // Ok, all content types match...
        if (acceptHeader.contains("*/*")) {
            return true;
        }
        // Edge case: we presume that if there's an accept header given, but no content type given
        // *and* no wildcard exists that we cannot fulfill the match...
        if (contentType == null) {
            return false;
        }
        // Remove the (optional) quality/weight tags...
        contentType = WEIGHT_PATTERN.matcher(contentType).replaceAll("");
        acceptHeader = WEIGHT_PATTERN.matcher(acceptHeader).replaceAll("");
        // Ok, the requested content type matches exactly...
        if (acceptHeader.contains(contentType)) {
            return true;
        }

        // Strictly speaking this is not the right way to do this, as the value can contain "q"
        // and other labels to give the media-type more weight or information. But for now it is
        // sufficient to just use the media types as-is...
        String[] acceptValues = acceptHeader.split("\\s*,\\s*");
        sort(acceptValues);

        return binarySearch(acceptValues, contentType.replaceFirst("/.*$", "/*")) > -1;
    }

    /**
     * Returns a map of all cookies send in the given servlet request.
     *
     * @param req the {@link HttpServletRequest} for which the cookies should be returned.
     * @return a map of cookies, in no particular order, using the name of the cookie as grouping key, never
     *         <code>null</code>.
     */
    public static Map<String, List<Cookie>> getCookieMap(HttpServletRequest req) {
        return stream(req.getCookies())
            .collect(groupingBy(Cookie::getName));
    }

    /**
     * @return the date value of the request header, if available, otherwise an empty optional.
     */
    public static Optional<Long> getDateHeader(HttpServletRequest req, String headerName) {
        long value = -1L;
        try {
            value = req.getDateHeader(headerName);
        } catch (IllegalArgumentException e) {
            // Ignore, use default value...
        }
        return (value < 0) ? Optional.empty() : Optional.of(value);
    }

    /**
     * Determines whether or not the given servlet request is secure.
     * <p>
     * The given servlet request is considered secure if it is calling a secure endpoint (e.g., secure http), or is
     * forwarded from a secure endpoint containing a "X-Forwarded-Proto" or "Forwarded" header.
     * </p>
     *
     * @param req the servlet request, cannot be <code>null</code>.
     * @return <code>true</code> if the given servlet request is secure (or a forwarded secure request),
     *         <code>false</code> otherwise.
     */
    public static boolean isSecureRequest(HttpServletRequest req) {
        String hdr;
        if (req.isSecure()) {
            return true;
        }
        // This is actually the de-facto, but deprecated, way of doing this...
        hdr = req.getHeader(X_FORWARDED_PROTO);
        if (HTTPS.equalsIgnoreCase(hdr)) {
            return true;
        }
        // The preferred way: https://tools.ietf.org/html/rfc7239#section-5,
        // not yet supported by many vendors...
        hdr = req.getHeader(FORWARDED);
        if (hdr != null) {
            return PROTO_HTTPS_MATCHES.matcher(hdr).find();
        }
        return false;
    }

    /**
     * @return <code>true</code> if the given request is a conditional request which must return a content_not_modified
     *         status code, <code>false</code> otherwise.
     */
    public static boolean notModified(HttpServletRequest req, Optional<ETag> eTag, OptionalLong lastModified) {
        String noMatch = req.getHeader(IF_NONE_MATCH);
        if (noMatch != null && eTag.isPresent()) {
            return matches(noMatch, eTag.get());
        }
        return getDateHeader(req, IF_MODIFIED_SINCE)
            .map(modifiedSince -> !modified(modifiedSince, lastModified))
            .orElse(Boolean.FALSE);
    }

    /**
     * @return <code>true</code> if the given request is a conditional request that must return a precondition_failed
     *         status code, <code>false</code> otherwise.
     */
    public static boolean preconditionFailed(HttpServletRequest req, Optional<ETag> eTag, OptionalLong lastModified) {
        String match = req.getHeader(IF_MATCH);
        if (match != null && eTag.isPresent()) {
            return !matches(match, eTag.get());
        }
        return getDateHeader(req, IF_UNMODIFIED_SINCE)
            .map(unmodifiedSince -> modified(unmodifiedSince, lastModified))
            .orElse(Boolean.FALSE);
    }

    /**
     * Returns <code>true</code> if the given match header matches the given ETag value.
     */
    static boolean matches(String matchHeader, ETag eTag) {
        if ("*".equals(matchHeader)) {
            return true;
        }
        return matchHeader.contains(eTag.getTag());
    }

    /**
     * @return <code>true</code> if the given modified header is less than the given last modified value (using 1 second
     *         of leeway), <code>false</code> otherwise.
     */
    static boolean modified(long modifiedHeader, OptionalLong lastModified) {
        if (!lastModified.isPresent()) {
            return false;
        }
        return (1L + (modifiedHeader / 1000L)) <= (lastModified.getAsLong() / 1000L);
    }
}
