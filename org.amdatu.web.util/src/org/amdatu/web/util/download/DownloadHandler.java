/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.util.download;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

/**
 * Provides a service that resolves a resource based on a given Servlet request.
 * <p>
 * Each download handler should return an instance of {@link DownloadResource} that provides information about this resource as
 * well access to its contents. Resources are expected to provide at least an entity-tag and name and may return their
 * complete length as well as their last modification date. Optionally, resources can also be downloaded as attachment.
 * </p>
 */
public interface DownloadHandler {

    /**
     * Returns the resource that matches the given HTTP request.
     * 
     * @param request the HTTP request to find the resource for, cannot be <code>null</code>.
     * @return an optional {@link DownloadResource} that provides access to the contents as well as metadata of the resource to
     *         download.
     */
    Optional<DownloadResource> getResource(HttpServletRequest request);

}
