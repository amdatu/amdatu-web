/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.util;

import static java.lang.Character.forDigit;
import static java.lang.Character.toUpperCase;
import static java.nio.charset.CodingErrorAction.REPORT;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.MalformedInputException;
import java.util.Base64;
import java.util.BitSet;

/**
 * Provides a encoder/decoder for representing URIs in various encodings.
 * <p>
 * This class provides a similar API as the {@link Base64} utility provided by default in the JDK and provides a
 * solution to the awkward and inconsistent implementation of {@link URLEncoder} and {@link URLDecoder}.
 * <p>
 * This utility class provides several implementations for encoding URIs, of which the default implementation (see
 * {@link #getEncoder()}/{@link #getDecoder()}) provides the same semantics as the <tt>encodeURIComponent</tt> and
 * <tt>decodeURIComponent</tt> present in JavaScript, which is safe to use in most situations (and not supported by,
 * for example, Apache Commons Codec).
 */
public class URICodec {

    /**
     * Decodes a previously encoded representation of an URI by replacing all of its escaped characters to their
     * unescaped representations.
     */
    public static interface Decoder {
        /**
         * Decodes the given string using the rules of this codec using UTF-8 as default character set.
         *
         * @param in the input string to decode, cannot be <code>null</code>.
         * @return the decoded input, never <code>null</code>.
         * @throws IllegalArgumentException in case the given parameter was <code>null</code>.
         */
        default String decode(String in) throws IllegalArgumentException {
            return decode(in, UTF_8);
        }

        /**
         * Decodes the given string using the rules of this codec using UTF-8 as default character set.
         *
         * @param in the input string to decode, cannot be <code>null</code>;
         * @param cs the character set of the output string, cannot be <code>null</code>.
         * @return the decoded input, never <code>null</code>.
         * @throws IllegalArgumentException in case one of the given parameters was <code>null</code>.
         */
        String decode(String in, Charset cs) throws IllegalArgumentException;
    }

    /**
     * Encodes a URI into a string representation by encoding all reserved characters by a string representation.
     */
    public static interface Encoder {
        /**
         * Encodes the given string using the rules of this codec using UTF-8 as default character set.
         *
         * @param in the input string to encode, cannot be <code>null</code>.
         * @return the encoded input, never <code>null</code>.
         * @throws IllegalArgumentException in case the given parameter was <code>null</code>.
         */
        default String encode(String in) throws IllegalArgumentException {
            return encode(in, UTF_8);
        }

        /**
         * Encodes the given string using the rules of this codec using UTF-8 as default character set.
         *
         * @param in the input string to encode, cannot be <code>null</code>;
         * @param cs the character set of the input string, cannot be <code>null</code>.
         * @return the encoded input, never <code>null</code>.
         * @throws IllegalArgumentException in case one of the given parameters was <code>null</code>.
         */
        String encode(String in, Charset cs) throws IllegalArgumentException;
    }

    /**
     * Generic base class for all exceptions that are coming from our encoder/decoder implementations.
     */
    public static class URICodecException extends RuntimeException {
        private static final long serialVersionUID = 1L;

        public URICodecException(String msg) {
            super(msg);
        }

        public URICodecException(String msg, Throwable cause) {
            super(msg, cause);
        }
    }

    /**
     * Thrown in case the URI is not encoded in the right way and as such cannot be decoded.
     */
    public static class MalformedURIException extends URICodecException {
        private static final long serialVersionUID = 1L;

        public MalformedURIException(String msg) {
            super(msg);
        }

        public MalformedURIException(String msg, Throwable cause) {
            super(msg, cause);
        }
    }

    /**
     * Default implementation of {@link Decoder}.
     */
    static class DecoderImpl implements Decoder {
        private final char[] m_replacements;

        DecoderImpl(char[] replacements) {
            m_replacements = replacements;
        }

        @Override
        public String decode(String in, Charset charset) throws IllegalArgumentException {
            if (in == null) {
                throw new IllegalArgumentException("Input cannot be null!");
            }
            if (charset == null) {
                throw new IllegalArgumentException("Charset cannot be null!");
            }

            int i = 0;
            int len = in.length();
            ByteBuffer buf = ByteBuffer.allocate(len);

            try {
                for (i = 0; i < len; i++) {
                    char ch = handleReplacement(in.charAt(i));
                    if (ch == '%') {
                        int u = Character.digit(in.charAt(++i), 16);
                        int l = Character.digit(in.charAt(++i), 16);
                        if (u == -1 || l == -1) {
                            throw new MalformedURIException("Malformed input at index: " + (i - 2));
                        }
                        buf.put((byte) ((u << 4) + l));
                    }
                    else {
                        buf.put((byte) ch);
                    }
                }
            }
            catch (StringIndexOutOfBoundsException e) {
                throw new MalformedURIException("Malformed input at index: " + i);
            }

            // We want to strictly decode every byte we've read, so in case of
            // malformed input or unmappable characters we want to have an exception...
            CharsetDecoder decoder = charset.newDecoder()
                .onMalformedInput(REPORT)
                .onUnmappableCharacter(REPORT);

            try {
                return decoder.decode((ByteBuffer) buf.flip()).toString();
            }
            catch (MalformedInputException e) {
                throw new MalformedURIException("Malformed input!", e);
            }
            catch (CharacterCodingException e) {
                throw new URICodecException("Invalid input!", e);
            }
        }

        private char handleReplacement(char in) {
            for (int i = 1; i < m_replacements.length; i += 2) {
                if (in == m_replacements[i]) {
                    return m_replacements[i - 1];
                }
            }
            return in;
        }
    }

    /**
     * Default implementation of {@link Encoder}.
     */
    static class EncoderImpl implements Encoder {
        private final BitSet m_safeChars;
        private final char[] m_replacements;

        EncoderImpl(BitSet safeChars, char[] replacements) {
            m_safeChars = safeChars;
            m_replacements = replacements;
        }

        @Override
        public String encode(String in, Charset charset) throws IllegalArgumentException {
            if (in == null) {
                throw new IllegalArgumentException("Input cannot be null!");
            }
            if (charset == null) {
                throw new IllegalArgumentException("Charset cannot be null!");
            }

            CharsetEncoder encoder = charset.newEncoder()
                .onMalformedInput(REPORT)
                .onUnmappableCharacter(REPORT);

            ByteBuffer bytes;
            try {
                bytes = encoder.encode(CharBuffer.wrap(in));
            }
            catch (CharacterCodingException e) {
                throw new URICodecException("Invalid input!", e);
            }

            int len = bytes.limit();

            StringBuilder out = new StringBuilder(len);

            for (int i = 0; i < len; i++) {
                byte b = bytes.get();
                if (m_safeChars.get(b & 0xFF)) {
                    out.append(handleReplacement(b));
                }
                else {
                    out.append('%');
                    out.append(toUpperCase(forDigit((b >> 4) & 0xF, 16)));
                    out.append(toUpperCase(forDigit(b & 0xF, 16)));
                }
            }

            return out.toString();
        }

        private char handleReplacement(byte in) {
            for (int i = 0; i < m_replacements.length; i += 2) {
                if (in == m_replacements[i]) {
                    return m_replacements[i + 1];
                }
            }
            return (char) in;
        }
    }

    static final BitSet RFC1738_UNRESERVED;
    static final BitSet RFC2396_UNRESERVED;
    static final BitSet RFC3986_UNRESERVED;

    private static final char[] WWW_FORM_URL_REPLACEMENTS = { ' ', '+' };
    private static final char[] NO_REPLACEMENTS = {};

    static {
        /*
         * RFC1738 states: Many URL schemes reserve certain characters for a special meaning: their appearance in the
         * scheme-specific part of the URL has a designated semantics. If the character corresponding to an octet is
         * reserved in a scheme, the octet must be encoded. The characters ";", "/", "?", ":", "@", "=" and "&" are the
         * characters which may be reserved for special meaning within a scheme. No other characters may be reserved
         * within a scheme.
         *
         * ...
         *
         * Thus, only alphanumerics, the special characters "$-_.+!*'(),", and reserved characters used for their
         * reserved purposes may be used unencoded within a URL.
         */
        RFC1738_UNRESERVED = createBasicBitSet();
        RFC1738_UNRESERVED.set('$');
        RFC1738_UNRESERVED.set('-');
        RFC1738_UNRESERVED.set('_');
        RFC1738_UNRESERVED.set('.');
        RFC1738_UNRESERVED.set(' '); // will be replaced!
        RFC1738_UNRESERVED.set('!');
        RFC1738_UNRESERVED.set('*');
        RFC1738_UNRESERVED.set('\'');
        RFC1738_UNRESERVED.set('(');
        RFC1738_UNRESERVED.set(')');
        RFC1738_UNRESERVED.set(',');

        /*
         * RFC2396 states: Data characters that are allowed in a URI but do not have a reserved purpose are called
         * unreserved. These include upper and lower case letters, decimal digits, and a limited set of punctuation
         * marks and symbols.
         *
         * unreserved = ALPHA / DIGIT / "-" / "_" / "." / "!" / "~" / "*" / "'" / "(" / ")"
         *
         * Unreserved characters can be escaped without changing the semantics of the URI, but this should not be done
         * unless the URI is being used in a context that does not allow the unescaped character to appear.
         */
        RFC2396_UNRESERVED = createBasicBitSet();
        RFC2396_UNRESERVED.set('-');
        RFC2396_UNRESERVED.set('_');
        RFC2396_UNRESERVED.set('.');
        RFC2396_UNRESERVED.set('!');
        RFC2396_UNRESERVED.set('~');
        RFC2396_UNRESERVED.set('*');
        RFC2396_UNRESERVED.set('\'');
        RFC2396_UNRESERVED.set('(');
        RFC2396_UNRESERVED.set(')');

        /*
         * RFC3986 states: Characters that are allowed in a URI but do not have a reserved purpose are called
         * unreserved. These include uppercase and lowercase letters, decimal digits, hyphen, period, underscore, and
         * tilde.
         *
         * unreserved = ALPHA / DIGIT / "-" / "." / "_" / "~"
         */
        RFC3986_UNRESERVED = createBasicBitSet();
        RFC3986_UNRESERVED.set('-');
        RFC3986_UNRESERVED.set('.');
        RFC3986_UNRESERVED.set('_');
        RFC3986_UNRESERVED.set('~');
    }

    /**
     * @return a codec instance suitable for decoding strings using the generic URL coding format, as specified in
     *         RFC2396 (which equals to the same behaviour as `decodeURIComponent` function from JavaScript).
     */
    public static URICodec.Decoder getDecoder() {
        return new DecoderImpl(NO_REPLACEMENTS);
    }

    /**
     * @return a codec instance suitable for encoding strings using the generic URL coding format, as specified in
     *         RFC2396 (which equals to the same behaviour as `encodeURIComponent` function from JavaScript).
     */
    public static URICodec.Encoder getEncoder() {
        return new EncoderImpl(RFC2396_UNRESERVED, NO_REPLACEMENTS);
    }

    /**
     * @return a codec instance suitable for decoding strings using the www-form-urlencoded format, as specified in HTML
     *         4.01 and RFC1738s.
     */
    public static URICodec.Decoder getFormUrlDecoder() {
        return new DecoderImpl(WWW_FORM_URL_REPLACEMENTS);
    }

    /**
     * @return a codec instance suitable for encoding strings using the www-form-urlencoded format, as specified in HTML
     *         4.01 and RFC1738.
     */
    public static URICodec.Encoder getFormUrlEncoder() {
        return new EncoderImpl(RFC1738_UNRESERVED, WWW_FORM_URL_REPLACEMENTS);
    }

    /**
     * @return a codec instance suitable for decoding strings using the "strict" URL encoding format, as specified in
     *         RFC3986.
     */
    public static URICodec.Decoder getStrictDecoder() {
        return new DecoderImpl(NO_REPLACEMENTS);
    }

    /**
     * @return a codec instance suitable for encoding strings using the "strict" URL encoding format, as specified in
     *         RFC3986.
     */
    public static URICodec.Encoder getStrictEncoder() {
        return new EncoderImpl(RFC3986_UNRESERVED, NO_REPLACEMENTS);
    }

    private static BitSet createBasicBitSet() {
        BitSet bitSet = new BitSet(256);
        int i;
        for (i = 'a'; i <= 'z'; i++) {
            bitSet.set(i);
        }
        for (i = 'A'; i <= 'Z'; i++) {
            bitSet.set(i);
        }
        for (i = '0'; i <= '9'; i++) {
            bitSet.set(i);
        }
        return bitSet;
    }

    /**
     * You can't call the constructor.
     */
    private URICodec() {
        // Not supposed to be instantiated.
    }
}
