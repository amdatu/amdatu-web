/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.spi;

import org.osgi.framework.ServiceReference;

import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.core.Application;

/**
 * Service that represents a JAX-RS application that should be published
 */
public interface ApplicationService {

    /**
     * The {@link Application} for this application service
     *
     * @return The {@link Application} for this application service service.
     */
    Application getApplication();

    /**
     * Get the name of this the application.
     *
     * @see org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants#JAX_RS_NAME
     * @return the name
     */
    String getName();

    /**
     * Get JAX-RS extension services registerd with the JAX-RS whiteboard for this {@link Application}
     *
     * @see ServiceReference#compareTo(Object)
     *
     * @return List of extension services for this application
     */
    List<? extends ApplicationExtension> getExtensions();

    /**
     * Get JAX-RS resources registerd with the JAX-RS whiteboard for this {@link Application}
     *
     * @see ServiceReference#compareTo(Object)
     *
     * @return List of resources for this application
     */
    List<? extends ApplicationResource> getResources();

    /**
     * The base URI for this application service
     *
     * @return the base URI never null
     */
    String getBaseUri();

    /**
     * The name of the {@link ServletContext} for this application service
     *
     * @return name of the {@link ServletContext} never null
     */
    String getServletContextName();

    void addChangeListener(ApplicationServiceChangedListener listener);

    void removeChangeListener(ApplicationServiceChangedListener listener);
}
