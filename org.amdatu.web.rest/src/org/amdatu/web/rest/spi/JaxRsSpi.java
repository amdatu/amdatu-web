/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.spi;

/**
 * Marker interface used to signal the 'readiness' of our JAS RS spi.
 * <p>
 * Some Java APIs rely on static class registration. The JAX-RS and the Crypto APIs are examples
 * of mechanisms that require the registration of service providers.
 * </p>
 * <p>
 * This is a dependency mechanism that works outside of the OSGi service space. For instance, when
 * your into a <tt>NoClassDefFoundError</tt> on <tt>com.sun.ws.rs.ext.RuntimeDelegateImpl</tt>
 * error, you probably do something with JAX-RS classes before the right SPI is registered. This
 * problem shows up as soon as the classes that use the API are loaded, not when they're started.
 * </p>
 * <p>
 * To link this SPI with the OSGi 'service world', we introduce marker interfaces for these SPIs.
 * </p>
 * <p>
 * (taken from: <tt>http://www.amdatu.org/confluence/display/Amdatu/JRE+SPI+integration</tt>)
 * </p>
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public interface JaxRsSpi {
}
