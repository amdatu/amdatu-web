/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.swagger;

import static org.amdatu.web.rest.doc.swagger.Constants.HEADER_X_FORWARDED_PROTO;
import static org.amdatu.web.rest.doc.swagger.SwaggerUtil.getDescription;
import static org.amdatu.web.rest.doc.swagger.SwaggerUtil.getPath;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_NAME;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

import org.amdatu.web.rest.doc.Description;
import org.amdatu.web.rest.doc.swagger.model.SwaggerAPI;
import org.amdatu.web.rest.doc.swagger.model.SwaggerResource;
import org.amdatu.web.rest.doc.swagger.model.SwaggerResources;
import org.amdatu.web.rest.spi.ApplicationService;
import org.amdatu.web.rest.spi.ApplicationResource;
import org.apache.felix.dm.Component;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.context.ServletContextHelper;
import org.osgi.service.http.whiteboard.HttpWhiteboardConstants;

import com.google.gson.Gson;

/**
 * Listens to services in the framework and analyzes them for documentation. Also implements
 * the Servlet API so you can map this component to an endpoint and use it to show all
 * REST APIs that are available in the framework.
 */
@SuppressWarnings("serial")
public class SwaggerServlet extends HttpServlet {

    private final CopyOnWriteArrayList<ApplicationService> m_restServices = new CopyOnWriteArrayList<ApplicationService>();

    public void addService(ServiceReference<ApplicationService> ref, ApplicationService service) {
        m_restServices.addIfAbsent(service);
    }

    public void removeService(ServiceReference<Object> ref, Object service) {
        m_restServices.remove(service);
    }

    protected SwaggerResources createResourceListingFor(String baseURL, String rootPath, Map<String, Class<?>> services) {
        List<SwaggerResource> rs = new ArrayList<SwaggerResource>();
        for (Entry<String, Class<?>> entry : services.entrySet()) {
            Class<?> serviceType = entry.getValue();

            // Check if service is annotated with @Path, or if implementing an interface which has @Path.
            if (getTypeWithPathAnnotation(serviceType).isPresent()) {
                String path = getPath(entry.getKey());
                String doc = getDescription(serviceType.getAnnotation(Description.class));

                rs.add(new SwaggerResource(rootPath.concat(path), doc));
            }
        }
        return new SwaggerResources(baseURL, rs);
    }

    /**
     * Called by Felix DM when stopping this component.
     */
    protected final void dmStop(Component comp) {
        // Avoid unwanted leakage of resources...
        m_restServices.clear();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String baseURL = getBaseURL(req);
        String requestPath = getPath(req.getPathInfo());

        SortedMap<String, Class<?>> services = getServices();
        if ("".equals(requestPath) || "/".equals(requestPath)) {
            SwaggerResources resources = createResourceListingFor(baseURL, req.getContextPath().concat(req.getServletPath()), services);

            writeAsJSON(resp, resources);
        } else {
            Class<?> serviceType = services.get(requestPath);
            if (serviceType != null) {
                SwaggerAPI apiDocs = SwaggerUtil.createDocumentationFor(baseURL, requestPath, serviceType);

                writeAsJSON(resp, apiDocs);
            } else {
                // Swagger-UI b0rks when returning anything other than a valid JSON response,
                // so in case we didn't write anything, simply return an empty JSON string...
                writeAsJSON(resp, "");
            }
        }
    }

    private String getBaseURL(HttpServletRequest request) {
        int port = request.getServerPort();

        String protocol = request.getHeader(HEADER_X_FORWARDED_PROTO);
        if (protocol != null) {
            if ("https".equals(protocol)) {
                port = 443;
            }
        } else {
            protocol = request.isSecure() ? "https" : "http";
        }

        return protocol + "://" + request.getServerName() + ":" + port;
    }

    private SortedMap<String, Class<?>> getServices() {
        SortedMap<String, Class<?>> result = new TreeMap<>();
        for (ApplicationService applicationService : m_restServices) {
            String baseUri = getRestServletBaseUri(applicationService);
            for (ApplicationResource obj : applicationService.getResources()) {
                final Object service = obj.getService();
                // If the service itself has no @Path annotation, try finding an implemented interface that has one
                // If neither the service nor one of its interfaces has @Path, throw an exception. This should not happen.
                Class<?> serviceType = getTypeWithPathAnnotation(service.getClass())
                        .orElseThrow(() ->
                            new RuntimeException(String.format("Service %s has no @Path annotation, and does not implement any interface which has.", service.getClass().getCanonicalName())
                        ));

                String path = getPath(serviceType.getAnnotation(Path.class));
                result.put(baseUri.concat(path), serviceType);
            }
        }
        return result;
    }

    private String getRestServletBaseUri(ApplicationService applicationService) {
        String restServletBaseUri;
        try {
            Collection<ServiceReference<ServletContextHelper>> serviceReferences = FrameworkUtil.getBundle(getClass())
                    .getBundleContext()
                    .getServiceReferences(ServletContextHelper.class, String.format("(%s=%s)", HTTP_WHITEBOARD_CONTEXT_NAME, applicationService.getServletContextName()));
            restServletBaseUri = serviceReferences.stream()
                    .sorted(Comparator.reverseOrder())
                    .findFirst()
                    .map(ref -> (String)ref.getProperty(HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_PATH))
                    .orElse(null);
        }
        catch (InvalidSyntaxException e) {
            // Should not happen as this
            throw new RuntimeException(e);
        }

        if (restServletBaseUri == null) {
          restServletBaseUri = "";
        } if (restServletBaseUri.endsWith("/")) {
            restServletBaseUri = restServletBaseUri.substring(0, restServletBaseUri.length() -1);
        }
        restServletBaseUri = restServletBaseUri.concat(applicationService.getBaseUri());
        return restServletBaseUri;
    }

    private void writeAsJSON(HttpServletResponse resp, Object object) throws IOException {
        Gson gson = new Gson();
        resp.setContentType(MediaType.APPLICATION_JSON);
        PrintWriter writer = resp.getWriter();
        try {
            writer.append(gson.toJson(object));
        } finally {
            writer.flush();
            writer.close();
        }
    }

    private Optional<Class<?>> getTypeWithPathAnnotation(Class<?> serviceType) {
        if (serviceType.isAnnotationPresent(Path.class)) {
            return Optional.of(serviceType);
        } else {
            // Check implemented interfaces for @Path, return first
            return Arrays
                    .stream(serviceType.getInterfaces())
                    .filter(i -> i.isAnnotationPresent(Path.class))
                    .findFirst();
        }
    }

}