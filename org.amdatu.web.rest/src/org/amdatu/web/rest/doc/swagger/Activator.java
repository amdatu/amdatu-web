/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.swagger;

import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_NAME;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_PATH;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_SELECT;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_SERVLET_PATTERN;

import java.util.Dictionary;
import java.util.Properties;

import javax.servlet.Servlet;

import org.amdatu.web.rest.spi.ApplicationService;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.http.context.ServletContextHelper;

public class Activator extends DependencyActivatorBase {

    public static final String PID = "org.amdatu.web.rest.doc.swagger";
    public static final String CTX_NAME_SWAGGER = "org.amdatu.web.rest.doc.swagger";

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        Properties props = new Properties();
        props.put(Constants.SERVICE_PID, PID);
        manager.add(createComponent().setInterface(ManagedService.class.getName(), props)
            .setImplementation(new ServletContextHelperRegistrationHelper())
            .setAutoConfig(Component.class, false));


        props = new Properties();
        props.put(HTTP_WHITEBOARD_CONTEXT_SELECT, String.format("(%s=%s)", HTTP_WHITEBOARD_CONTEXT_NAME, CTX_NAME_SWAGGER));
        props.put(HTTP_WHITEBOARD_SERVLET_PATTERN, "/docs/*");

        manager.add(createComponent()
            .setInterface(Servlet.class.getName(), props)
            .setImplementation(SwaggerServlet.class)
            .setCallbacks("dmInit", "dmStart", "dmStop", "dmDestroy") // avoid clashes with Servlet API
            .add(createServiceDependency()
                .setService(ApplicationService.class).setCallbacks("addService", "removeService").setRequired(false))
        );
    }

    private static class ServletContextHelperRegistrationHelper implements ManagedService {

        private volatile DependencyManager m_manager;

        private Component m_component;

        /**
         * Called by Felix DM when the component is started
         */
        @SuppressWarnings("unused")
        protected final void start() {
            Properties props = new Properties();
            props.put(Constants.SERVICE_PID, PID);
            props.put(HTTP_WHITEBOARD_CONTEXT_NAME, CTX_NAME_SWAGGER);
            m_component = m_manager.createComponent()
                .setInterface(ServletContextHelper.class.getName(), props)
                .setImplementation(SwaggerServletContextHelperFactory.class);
            m_manager.add(m_component);
        }

        /**
         * Called by Felix DM when the component is stopped
         */
        @SuppressWarnings("unused")
        protected final void stop() {
            if (m_component != null) {
                m_manager.remove(m_component);
            }
        }

        @Override
        public void updated(Dictionary<String, ?> properties) throws ConfigurationException {
            Object path = null;

            if (properties != null) {
                path = properties.get("path");
            }

            if (!(path instanceof String)) {
                path = "/swagger";
            }

            Dictionary<Object,Object> serviceProperties = m_component.getServiceProperties();
            serviceProperties.put(HTTP_WHITEBOARD_CONTEXT_PATH, path);
            m_component.setServiceProperties(serviceProperties);
        }

    }
}
