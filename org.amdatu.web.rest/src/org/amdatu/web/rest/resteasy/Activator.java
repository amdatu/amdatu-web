/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.resteasy;

import org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants;
import org.amdatu.web.rest.jaxrs.client.AmdatuClientBuilder;
import org.amdatu.web.rest.whiteboard.JaxrsWhiteboard;
import org.amdatu.web.rest.resteasy.client.AmdatuClientBuilderServiceFactory;
import org.amdatu.web.rest.resteasy.client.ClientBuilderServiceFactory;
import org.amdatu.web.rest.spi.ApplicationService;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.service.http.context.ServletContextHelper;
import org.osgi.service.http.whiteboard.HttpWhiteboardConstants;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;
import org.osgi.service.log.LogService;

import javax.servlet.Servlet;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Application;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.RuntimeDelegate;
import java.util.Dictionary;
import java.util.Hashtable;

import static java.lang.String.format;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_NAME;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_DEFAULT_APPLICATION;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_EXTENSION;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_NAME;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_RESOURCE;

public class Activator extends DependencyActivatorBase {

    @Override
    public void init(BundleContext context, DependencyManager manager) {
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        try {
            // set context class loader for service loading to work
            Thread.currentThread().setContextClassLoader(getClass().getClassLoader());

            // Initialize the runtime delegate (singleton) to make sure the instance is available
            RuntimeDelegate.getInstance();
        } finally {
            Thread.currentThread().setContextClassLoader(contextClassLoader);
        }


        manager.add(createComponent()
                .setInterface(JaxrsWhiteboard.class.getName(), null)
                .setImplementation(JaxrsWhiteboard.class)
                // Workaround for FELIX-5268: Service not unregistered while bundle is starting
                .add(createBundleDependency()
                        .setBundle(context.getBundle())
                        .setStateMask(Bundle.ACTIVE)
                        .setRequired(true))
                .add(createServiceDependency()
                        .setService(LogService.class)
                        .setRequired(false))
                .add(createServiceDependency()
                        .setService(Application.class, format("(%s=*)", JAX_RS_APPLICATION_BASE))
                        .setCallbacks("applicationAdded", "applicationChanged", "applicationRemoved")
                        .setRequired(false))
                .add(createServiceDependency()
                        .setService(format("(%s=%s)", JAX_RS_RESOURCE, "true"))
                        .setCallbacks("resourceAdded", "resourceChanged", "resourceRemoved")
                        .setRequired(false))
                .add(createServiceDependency()
                        .setService(format("(%s=%s)", JAX_RS_EXTENSION, "true"))
                        .setCallbacks("extensionAdded", "extensionChanged", "extensionRemoved")
                        .setRequired(false))
                .add(createServiceDependency()
                        .setService(ServletContextHelper.class, format("(%s=*)", HTTP_WHITEBOARD_CONTEXT_NAME))
                        .setCallbacks("servletContextAdded", "servletContextChanged", "servletContextRemoved")
                        .setRequired(false))
        );

        Dictionary<String, Object> defaultApplicationProperties = new Hashtable<>();
        defaultApplicationProperties.put(JAX_RS_NAME, JAX_RS_DEFAULT_APPLICATION);
        defaultApplicationProperties.put(JAX_RS_APPLICATION_BASE, "/rest");
        manager.add(createComponent()
                .setInterface(Application.class.getName(), defaultApplicationProperties)
                .setImplementation(new Application()));

        manager.add(createAdapterService(ApplicationService.class, null, "onAdd", null, null)
                .setInterface(Servlet.class.getName(), null)
                .setImplementation(OsgiHttpServletDispatcher.class)
                .setCallbacks("dmInit", "dmStart", "dmStop", null)
        );

        manager.add(createComponent()
                .setInterface(ClientBuilder.class.getName(), null)
                .setImplementation(ClientBuilderServiceFactory.class)
        );

        manager.add(createComponent()
                .setInterface(AmdatuClientBuilder.class.getName(), null)
                .setImplementation(AmdatuClientBuilderServiceFactory.class)
        );

        Dictionary<String, Object> exceptionHashMapperProperties = new Hashtable<>();
        exceptionHashMapperProperties.put(JaxrsWhiteboardConstants.JAX_RS_EXTENSION, true);
        exceptionHashMapperProperties.put(JaxrsWhiteboardConstants.JAX_RS_NAME, "ExceptionHashMapper");
        exceptionHashMapperProperties.put(JaxrsWhiteboardConstants.JAX_RS_APPLICATION_SELECT, String.format("(!(%s=false))", AmdatuWebRestConstants.EXCEPTION_MAPPERS_ENABLED));
        manager.add(createComponent()
                .setInterface(ExceptionMapper.class.getName(), exceptionHashMapperProperties)
                .setImplementation(ExceptionHashMapper.class));

        Dictionary<String, Object> webApplicationExceptionMapperProperties = new Hashtable<>();
        webApplicationExceptionMapperProperties.put(JaxrsWhiteboardConstants.JAX_RS_EXTENSION, true);
        webApplicationExceptionMapperProperties.put(JaxrsWhiteboardConstants.JAX_RS_NAME, "WebApplicationExceptionMapper");
        webApplicationExceptionMapperProperties.put(JaxrsWhiteboardConstants.JAX_RS_APPLICATION_SELECT, String.format("(!(%s=false))", AmdatuWebRestConstants.EXCEPTION_MAPPERS_ENABLED));
        manager.add(createComponent()
                .setInterface(ExceptionMapper.class.getName(), webApplicationExceptionMapperProperties)
                .setImplementation(WebApplicationExceptionMapper.class));
    }
}
