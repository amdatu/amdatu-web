/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.resteasy;

import org.osgi.service.http.context.ServletContextHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * ExceptionMapper for WebApplicationExceptions to prevent them from being handled by the GenericExceptionMapper
 */
@Provider
public class WebApplicationExceptionMapper implements ExceptionMapper<WebApplicationException> {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHashMapper.class);

	private volatile @Context HttpServletRequest request;

	@Override
	public Response toResponse(WebApplicationException e) {
		final String userId = (String) request.getAttribute(ServletContextHelper.REMOTE_USER);
		int status = e.getResponse().getStatus();
		// Error range 4xx client-, 5xx server-errors
		if (status >= 400 && status < 600) {
			LOGGER.warn("Uncaught exception for request: {} by userId: {} error: {} {} {}", request.getRequestURI(), userId, e.getMessage(), request.getRemoteAddr(), request.getHeader("X-Forwarded-For"));
		}
		return e.getResponse();
	}
}