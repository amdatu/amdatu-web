/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.resteasy;

import org.osgi.service.http.context.ServletContextHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;

@Provider
public class ExceptionHashMapper implements ExceptionMapper<Throwable> {

    private static final int TAG_LENGTH = 6;

    private volatile @Context HttpServletRequest request;
    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHashMapper.class);

    private volatile AtomicInteger counter = new AtomicInteger();

    public Response toResponse(Throwable e) {
        final String tag = calculateTag(e);

        final String msgTag = tag + "-" + String.format("%04d", counter.incrementAndGet());
        final String userId = (String) request.getAttribute(ServletContextHelper.REMOTE_USER);

        LOGGER.warn("Uncaught exception for request: {} by userId: {} error: {} {} {}", request.getRequestURI(), userId, msgTag, request.getRemoteAddr(), request.getHeader("X-Forwarded-For"), e);

        Response.ResponseBuilder responseBuilder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        responseBuilder.header("Expires", null);
        responseBuilder.header("Last-Modified", null);
        responseBuilder.header("Cache-Control", "must-revalidate,no-cache,no-store");
        responseBuilder.header("Content-Type", "application/json");
        responseBuilder.entity("{\"error\" : \"" + msgTag + "\"}");

        return responseBuilder.build();
    }

    /** Calculates a tag based on the exception given. So the same exception will have the same tag. There is a small chance for collision here, but that does not matter */
    private String calculateTag(Throwable e)  {
        try {
            StackTraceElement[] stacktrace = e.getStackTrace();
            String origin = stacktrace != null && stacktrace.length > 0 ? e.getStackTrace()[0].toString() : "";

            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(origin.getBytes("UTF-8"));
            byte[] digest = md.digest();
            String stackHash = String.format("%064x", new java.math.BigInteger(1, digest));
            // now remove all numbers, to reduce confusion like AA5A or AASA
            stackHash = stackHash.replaceAll("\\d", "");
            return stackHash.substring(0, Math.min(TAG_LENGTH, stackHash.length() - 1)).toUpperCase(Locale.US);
        }
        catch (UnsupportedEncodingException | NoSuchAlgorithmException e1) {
            return "XXXXXX";
        }
    }

}
