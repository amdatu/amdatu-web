/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.jaxrs;

import javax.ws.rs.core.Application;

import org.osgi.service.http.context.ServletContextHelper;
import org.osgi.service.http.whiteboard.HttpWhiteboardConstants;

/**
 * Defines constants for the Amdatu-Web REST whiteboard services.
 */
public class AmdatuWebRestConstants {

    private AmdatuWebRestConstants() {
        // non-instantiable
    }

    /**
     * Service property specifying the name of a JAX-RS resource.
     * <p>
     * Resource names should be unique among all resource service associated with a single whiteboard implementation.
     * <p>
     * The value of this service property must be of type String.
     *
     * Use {@link org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants#JAX_RS_NAME}
     */
    @Deprecated
    public static final String JAX_RS_RESOURCE_NAME = "osgi.jaxrs.name";

    /**
     * Service property specifying the base URI mapping for a JAX-RS application service, which implies that
     * all registered resources that belong to that application will be prefixed as well.
     * <p>
     * The value of this service property must be of type String, and will have a "/" prepended if no "/" exists.
     * If omitted, an empty value is used (this means no prefix will be used).
     *
     * Use {@link org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants#JAX_RS_APPLICATION_BASE}
     */
    @Deprecated
    public static final String JAX_RS_APPLICATION_BASE = "osgi.jaxrs.application.base";

    /**
     * Service property specifying the name of the {@link ServletContextHelper} that should be used for the JAX-RS
     * application service.
     * <p>
     * The value of this service property must be of type String and should be provided for the {@link Application}
     * service. If omitted, the value {@link HttpWhiteboardConstants#HTTP_WHITEBOARD_DEFAULT_CONTEXT_NAME} will be
     * used as default.
     */
    public static final String JAX_RS_APPLICATION_CONTEXT = "org.amdatu.web.rest.application.context";

    /**
     * Service property specifying the name for a JAX-RS application.
     * <p>
     * The value of this service property must be of type String and should be provided on <b>both</b> the
     * {@link Application} and all REST endpoints that belongs to that application.
     * <p>
     *
     * Use {@link org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants#JAX_RS_NAME}
     */
    @Deprecated
    public static final String JAX_RS_APPLICATION_NAME = "org.amdatu.web.rest.application.name";

    /**
     * Service property specifying whether the default amdatu web exception mappers should be enabled.
     * <p>
     *     The exception mappers are enabled by default. This service property can be used to disable them.
     * </p>
     */
    public static final String EXCEPTION_MAPPERS_ENABLED = "org.amdatu.web.exceptionmappers.enabled";
}
