/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.shell;

import org.osgi.service.jaxrs.runtime.JaxrsServiceRuntime;
import org.osgi.service.jaxrs.runtime.dto.ApplicationDTO;
import org.osgi.service.jaxrs.runtime.dto.DTOConstants;
import org.osgi.service.jaxrs.runtime.dto.ExtensionDTO;
import org.osgi.service.jaxrs.runtime.dto.FailedApplicationDTO;
import org.osgi.service.jaxrs.runtime.dto.ResourceDTO;
import org.osgi.service.jaxrs.runtime.dto.RuntimeDTO;

import java.util.*;
import java.util.function.Predicate;

public class WhiteboardCommands {

    private volatile JaxrsServiceRuntime m_jaxrsServiceRuntime;

    public void status() {
        RuntimeDTO runtimeDTO = m_jaxrsServiceRuntime.getRuntimeDTO();
        if (runtimeDTO == null) {
            System.out.println("RuntimeDTO not available");
            return;
        }

        System.out.println("Active JAX-RS Applications");
        System.out.println("--------------------------------------");
        String format = "%-5s | %-20s | %s";
        System.out.println(String.format(format, "id", "base", "name"));

        for (ApplicationDTO applicationDTO : runtimeDTO.applicationDTOs) {
            System.out.println(String.format(format,
                    applicationDTO.serviceId,
                    applicationDTO.base,
                    applicationDTO.name));
        }

        System.out.println();
        System.out.println("Failed JAX-RS Applications");
        System.out.println("--------------------------------------");

        format = "%-5s | %-20s | %-20s | %s";
        System.out.println(String.format(format, "id", "base", "name", "reason"));

        for (FailedApplicationDTO failedApplicationDTO : runtimeDTO.failedApplicationDTOs) {
            System.out.println(String.format(format,
                    failedApplicationDTO.serviceId,
                    failedApplicationDTO.base,
                    failedApplicationDTO.name,
                    failureReasonString(failedApplicationDTO.failureReason)));
        }

    }

    public void status(long serviceId) {
        printStatus(dto -> dto.serviceId == serviceId);
    }

    public void status(String baseUri) {
        printStatus(dto -> dto.base.equals(baseUri));
    }

    private void printStatus(Predicate<ApplicationDTO> predicate) {
        RuntimeDTO runtimeDTO = m_jaxrsServiceRuntime.getRuntimeDTO();

        ApplicationDTO applicationDTO = Arrays.stream(runtimeDTO.applicationDTOs)
                .filter(predicate)
                .findAny()
                .orElse(null);

        if (applicationDTO == null) {
            System.out.println("not found");
            return;
        }

        System.out.println("Resources");
        String format = "%-5s | %-20s | %s";
        System.out.println(String.format(format, "id", "path", "name"));

        for (ResourceDTO resourceDTO : applicationDTO.resourceDTOs) {
            System.out.println(String.format(format,
                    resourceDTO.serviceId,
                    "<<TODO>>",
                    resourceDTO.name));
        }


        System.out.println("Extensions");

        String extensionFormat = "%-5s | %-15s | %s";
        System.out.println(String.format(extensionFormat, "id", "name", "ext class"));

        for (ExtensionDTO extensionDTO : applicationDTO.extensionDTOs) {
            for (int i = 0; i < extensionDTO.extensionTypes.length; i++) {
                System.out.println(String.format(extensionFormat,
                        i == 0 ? extensionDTO.serviceId : "",
                        extensionDTO.name,
                        extensionDTO.extensionTypes[i]));
            }
        }
    }

    private String failureReasonString(int failureReason) {

        switch (failureReason) {
            case DTOConstants.FAILURE_REASON_VALIDATION_FAILED:
                return "Validation failed";
            case DTOConstants.FAILURE_REASON_REQUIRED_EXTENSIONS_UNAVAILABLE:
                return "Required extensions unavailable";
            case DTOConstants.FAILURE_REASON_SHADOWED_BY_OTHER_SERVICE:
                return "Shadowed by other service";
            case DTOConstants.FAILURE_REASON_REQUIRED_APPLICATION_UNAVAILABLE:
                return "Required applicaiton unavailable";
            case DTOConstants.FAILURE_REASON_NOT_AN_EXTENSION_TYPE:
                return "Not an extension type";
            case DTOConstants.FAILURE_REASON_DUPLICATE_NAME:
                return "Duplicate name";
            case DTOConstants.FAILURE_REASON_SERVICE_NOT_GETTABLE:
                return "Service not gettable";
            case DTOConstants.FAILURE_REASON_UNKNOWN:
            default:
                return "Unknown (" + failureReason + ")";
        }
    }

}
