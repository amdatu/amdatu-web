/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.whiteboard;

import java.util.Set;

import org.osgi.framework.Filter;
import org.osgi.framework.ServiceReference;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

public abstract class AbstractHolder {

    private final ServiceReference<?> m_serviceReference;
    private final Object m_service;

    private final Set<Filter> m_extensionSelect;

    public AbstractHolder(ServiceReference<?> serviceReference, Object service, Set<Filter> extensionSelect) {
        m_serviceReference = serviceReference;
        m_service = service;
        m_extensionSelect = extensionSelect;
    }

    public ServiceReference<?> getServiceReference() {
        return m_serviceReference;
    }

    public Object getService() {
        return m_service;
    }

    public Set<Filter> getExtensionSelect() {
        return m_extensionSelect;
    }

    public String getName() {
        return (String) getServiceReference().getProperty(JaxrsWhiteboardConstants.JAX_RS_NAME);
    }
}
