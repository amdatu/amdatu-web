/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.whiteboard;

import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_EXTENSION_SELECT;

public class JaxrsWhiteboardUtils {

    private final static String BSN_REGEX = "[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*";
    private final static Pattern BSN_PATTERN = Pattern.compile(BSN_REGEX);

    public static Set<Filter> getExtensionSelectFilters(ServiceReference<?> serviceReference) throws InvalidSyntaxException {
        Set<Filter> filters;
        List<String> filterStrings = getStringPlusProperty(serviceReference, JAX_RS_EXTENSION_SELECT);
        if (filterStrings.isEmpty()) {
            filters = Collections.emptySet();
        }
        else {
            filters = new HashSet<>();
            for (String filterString : filterStrings) {
                Filter filter = FrameworkUtil.createFilter(filterString);
                filters.add(filter);
            }
        }
        return filters;
    }

    private static List<String> getStringPlusProperty(ServiceReference<?> serviceReference, String propertyKey) {
        Object property = serviceReference.getProperty(propertyKey);

        if (property == null) {
            return Collections.emptyList();
        }

        List<String> result = new ArrayList<>();
        if (property instanceof String) {
            result.add((String) property);
        }
        else if (property instanceof String[]) {
            result.addAll(Arrays.asList((String[]) property));
        }
        else if (property instanceof Collection) {
            Collection collectionProperty = (Collection) property;
            for (Object o : collectionProperty) {
                if (property instanceof String) {
                    result.add((String) property);
                }
                else {
                    throw new RuntimeException(String.format(
                            "Invalid value for string plus property %s expected String, String[] or Collection<String> " +
                                    "got Collection containing %s", propertyKey, o.getClass()));
                }
            }
        }
        else {
            throw new RuntimeException(String.format(
                    "Invalid value for string plus property %s expected String, String[] or Collection<String> got %s",
                    propertyKey, property.getClass()));
        }
        return result;
    }

    public static boolean isValidSymbolicName(String name) {
        return BSN_PATTERN.matcher(name).matches();
    }

}
