/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.whiteboard.service;

import org.apache.felix.dm.Component;
import org.osgi.service.jaxrs.runtime.JaxrsServiceRuntime;
import org.osgi.service.jaxrs.runtime.dto.RuntimeDTO;

public class JaxrsServiceRuntimeImpl implements JaxrsServiceRuntime {

    private volatile Component m_component;
    private volatile RuntimeDTO m_runtimeDTO;

    @Override
    public RuntimeDTO getRuntimeDTO() {
        return m_runtimeDTO;
    }

    public void setRuntimeDTO(RuntimeDTO runtimeDTO) {
        m_runtimeDTO = runtimeDTO;
    }
}
