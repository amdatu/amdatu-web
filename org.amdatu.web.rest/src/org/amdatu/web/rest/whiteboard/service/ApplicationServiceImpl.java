/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.whiteboard.service;

import org.amdatu.web.rest.spi.ApplicationExtension;
import org.amdatu.web.rest.spi.ApplicationResource;
import org.amdatu.web.rest.spi.ApplicationService;
import org.amdatu.web.rest.spi.ApplicationServiceChangedListener;
import org.amdatu.web.rest.whiteboard.ApplicationHolder;
import org.apache.felix.dm.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Application;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ApplicationServiceImpl implements ApplicationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationServiceImpl.class);

    private final ApplicationHolder m_applicationHolder;

    private volatile Component m_component;

    private volatile List<ApplicationResource> m_applicationResources;
    private volatile List<ApplicationExtension> m_applicationExtensions;

    private final List<ApplicationServiceChangedListener> m_applicationServiceChangedListeners = new ArrayList<>();

    public ApplicationServiceImpl(ApplicationHolder applicationHolder) {
        m_applicationHolder = applicationHolder;
        update();
    }

    public ApplicationHolder getApplicationHolder() {
        return m_applicationHolder;
    }

    @Override
    public Application getApplication() {
        return m_applicationHolder.getApplication();
    }

    @Override
    public String getName() {
        return m_applicationHolder.getName();
    }

    @Override
    public List<? extends ApplicationExtension> getExtensions() {
        return m_applicationHolder.getExtensions().stream()
                .map(ApplicationExtensionImpl::new)
                .collect(Collectors.toList());
    }

    @Override
    public List<? extends ApplicationResource> getResources() {
        return m_applicationHolder.getResources().stream()
                .map(ApplicationResourceImpl::new)
                .collect(Collectors.toList());
    }

    @Override
    public String getBaseUri() {
        return m_applicationHolder.getBaseUri();
    }

    @Override
    public String getServletContextName() {
        return m_applicationHolder.getServletContextName();
    }

    @Override
    public void addChangeListener(ApplicationServiceChangedListener listener) {
        m_applicationServiceChangedListeners.add(listener);
    }

    @Override
    public void removeChangeListener(ApplicationServiceChangedListener listener) {
        m_applicationServiceChangedListeners.remove(listener);
    }

    public Component getComponent() {
        return m_component;
    }

    public void update() {
        boolean changed = false;
        synchronized (this) {
            List<ApplicationResource> newResourceList = m_applicationHolder.getResources().stream()
                    .map(ApplicationResourceImpl::new)
                    .collect(Collectors.toList());

            if (!newResourceList.equals(m_applicationResources)) {
                m_applicationResources = newResourceList;
                LOGGER.debug("Resources changed for application {}", m_applicationHolder.getName());

                changed = true;
            }

            List<ApplicationExtension> newExtensionList = m_applicationHolder.getExtensions().stream()
                    .map(ApplicationExtensionImpl::new)
                    .collect(Collectors.toList());

            if (!newExtensionList.equals(m_applicationExtensions)) {
                m_applicationExtensions = newExtensionList;
                LOGGER.debug("Extensions changed for application {}", m_applicationHolder.getName());
                changed = true;
            }
        }

        if (changed) {
            LOGGER.debug("Notifying '{}' listeners ", m_applicationServiceChangedListeners.size());
            m_applicationServiceChangedListeners.forEach(ApplicationServiceChangedListener::onChange);
        }
    }

}
