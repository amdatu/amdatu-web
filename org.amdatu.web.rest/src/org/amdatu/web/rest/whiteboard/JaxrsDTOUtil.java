/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.whiteboard;

import java.util.ArrayList;

import org.osgi.framework.Constants;
import org.osgi.framework.ServiceReference;
import org.osgi.service.jaxrs.runtime.dto.ApplicationDTO;
import org.osgi.service.jaxrs.runtime.dto.BaseDTO;
import org.osgi.service.jaxrs.runtime.dto.ExtensionDTO;
import org.osgi.service.jaxrs.runtime.dto.FailedApplicationDTO;
import org.osgi.service.jaxrs.runtime.dto.FailedExtensionDTO;
import org.osgi.service.jaxrs.runtime.dto.FailedResourceDTO;
import org.osgi.service.jaxrs.runtime.dto.ResourceDTO;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JaxrsDTOUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(JaxrsDTOUtil.class);

    public static ApplicationDTO createApplicationDTO(ApplicationHolder applicationHolder) {
        ArrayList<ResourceDTO> resourceDTOs = new ArrayList<>();
        for (ResourceHolder resource : applicationHolder.getResources()) {
            ResourceDTO resourceDTO = createResourceDTO(resource);

            resourceDTOs.add(resourceDTO);
        }

        ArrayList<ExtensionDTO> extensionDTOs = new ArrayList<>();
        for (ExtensionHolder extension : applicationHolder.getExtensions()) {
            ExtensionDTO extensionDTO = createExtensionDTO(extension);
            extensionDTOs.add(extensionDTO);
        }

        ApplicationDTO applicationDTO = createDto(applicationHolder, ApplicationDTO.class);
        applicationDTO.base = applicationHolder.getBaseUri();
        applicationDTO.resourceDTOs = resourceDTOs.toArray(new ResourceDTO[0]);
        applicationDTO.extensionDTOs = extensionDTOs.toArray(new ExtensionDTO[0]);
        return applicationDTO;
    }

    public static FailedApplicationDTO createFailedApplicationDTO(ServiceReference serviceReference, int reason) {
        FailedApplicationDTO failedApplicationDTO = createDto(serviceReference, FailedApplicationDTO.class);
        failedApplicationDTO.failureReason = reason;
        return failedApplicationDTO;
    }

    public static ResourceDTO createResourceDTO(ResourceHolder resource) {
        ResourceDTO resourceDTO = createDto(resource, ResourceDTO.class);

        // TODO: The ResourceDTO lacks information on the base path of the resource should we create a custom type to add that
        // TODO: ResourceMethodInfoDTO requires a lot of JAX-RS knowledge, skip that for now ..

        return resourceDTO;
    }

    public static FailedResourceDTO createFailedResourceDTO(ServiceReference<Object> serviceReference, int reason) {
        FailedResourceDTO failedResourceDTO = createDto(serviceReference, FailedResourceDTO.class);
        failedResourceDTO.failureReason = reason;
        return failedResourceDTO;
    }

    public static ExtensionDTO createExtensionDTO(ExtensionHolder extension) {
        ExtensionDTO extensionDTO = createDto(extension, ExtensionDTO.class);
        extensionDTO.extensionTypes = extension.getContracts().stream().map(Class::getName).toArray(String[]::new);
        return extensionDTO;
    }


    public static FailedExtensionDTO createFailedExtensionDTO(ServiceReference<Object> serviceReference, int reason) {
        FailedExtensionDTO failedExtensionDTO = createDto(serviceReference, FailedExtensionDTO.class);
        failedExtensionDTO.failureReason = reason;
        return failedExtensionDTO;
    }

    public static <T extends BaseDTO> T createDto(AbstractHolder holder, Class<T> dtoClass) {
        ServiceReference<?> serviceReference = holder.getServiceReference();
        T dto = createDto(holder.getServiceReference(), dtoClass);
        if (dto.name == null) {
            dto.name = "." + holder.getService().getClass().getName();
        }
        return dto;
    }

    public static <T extends BaseDTO> T createDto(ServiceReference<?> serviceReference, Class<T> dtoClass) {
        try {
            T dto = dtoClass.newInstance();
            dto.serviceId = (long) serviceReference.getProperty(Constants.SERVICE_ID);
            dto.name = (String) serviceReference.getProperty(JaxrsWhiteboardConstants.JAX_RS_NAME);
            return dto;
        }
        catch (InstantiationException | IllegalAccessException e) {
            LOGGER.error("Failed to create DTO", e);
            throw new RuntimeException(e);
        }
    }
}
