/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.whiteboard;

import java.util.Set;

import org.amdatu.web.rest.spi.ApplicationResource;
import org.osgi.framework.Filter;
import org.osgi.framework.ServiceReference;

public class ResourceHolder extends AbstractHolder implements ApplicationResource {

    private final Filter m_applicationSelect;

    public ResourceHolder(ServiceReference<?> serviceReference, Object service, Set<Filter> extensionSelect, Filter applicationSelect) {
        super(serviceReference, service, extensionSelect);
        m_applicationSelect = applicationSelect;
    }

    public Filter getApplicationSelect() {
        return m_applicationSelect;
    }
}
