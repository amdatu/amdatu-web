/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.whiteboard;

import java.util.Set;

import org.amdatu.web.rest.spi.ApplicationExtension;
import org.osgi.framework.Constants;
import org.osgi.framework.Filter;
import org.osgi.framework.ServiceReference;
import org.osgi.service.jaxrs.runtime.dto.ExtensionDTO;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

public class ExtensionHolder extends AbstractHolder {

    private final Filter m_applicationSelect;
    private final Set<Class<?>> m_contracts;

    public ExtensionHolder(ServiceReference<?> serviceReference, Object service, Set<Filter> extensionSelect, Filter applicationSelect, Set<Class<?>> contracts) {
        super(serviceReference, service, extensionSelect);
        m_applicationSelect = applicationSelect;
        m_contracts = contracts;
    }

    public Filter getApplicationSelect() {
        return m_applicationSelect;
    }

    public Set<Class<?>> getContracts() {
        return m_contracts;
    }

}
