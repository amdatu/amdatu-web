/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.whiteboard;

import static java.lang.String.format;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_CONTEXT;
import static org.amdatu.web.rest.whiteboard.JaxrsDTOUtil.createApplicationDTO;
import static org.amdatu.web.rest.whiteboard.JaxrsDTOUtil.createFailedApplicationDTO;
import static org.amdatu.web.rest.whiteboard.JaxrsDTOUtil.createFailedExtensionDTO;
import static org.amdatu.web.rest.whiteboard.JaxrsDTOUtil.createFailedResourceDTO;
import static org.amdatu.web.rest.whiteboard.JaxrsWhiteboardUtils.getExtensionSelectFilters;
import static org.osgi.framework.FrameworkUtil.createFilter;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_NAME;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_PATH;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_APPLICATION_SELECT;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_DEFAULT_APPLICATION;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_NAME;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.stream.Collectors;

import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Feature;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.ParamConverterProvider;
import javax.ws.rs.ext.ReaderInterceptor;
import javax.ws.rs.ext.WriterInterceptor;

import org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants;
import org.amdatu.web.rest.jaxrs.JaxRsRequestInterceptor;
import org.amdatu.web.rest.spi.ApplicationService;
import org.amdatu.web.rest.whiteboard.service.ApplicationServiceImpl;
import org.amdatu.web.rest.whiteboard.service.JaxrsServiceRuntimeImpl;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.Constants;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.context.ServletContextHelper;
import org.osgi.service.http.whiteboard.HttpWhiteboardConstants;
import org.osgi.service.jaxrs.runtime.JaxrsServiceRuntime;
import org.osgi.service.jaxrs.runtime.dto.ApplicationDTO;
import org.osgi.service.jaxrs.runtime.dto.DTOConstants;
import org.osgi.service.jaxrs.runtime.dto.FailedApplicationDTO;
import org.osgi.service.jaxrs.runtime.dto.FailedExtensionDTO;
import org.osgi.service.jaxrs.runtime.dto.FailedResourceDTO;
import org.osgi.service.jaxrs.runtime.dto.RuntimeDTO;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JaxrsWhiteboard {

    public static final String DEFAULT_APPLICATION_SELECT = "(" + JAX_RS_NAME + "=" + JAX_RS_DEFAULT_APPLICATION + ")";

    public static final List<Class<?>> SUPPORTED_EXTENSION_CONTRACTS = Arrays.asList(
            MessageBodyWriter.class,
            MessageBodyReader.class,
            ContainerRequestFilter.class,
            ContainerResponseFilter.class,
            ReaderInterceptor.class,
            WriterInterceptor.class,
            ContextResolver.class,
            ExceptionMapper.class,
            ParamConverterProvider.class,
            Feature.class,
            DynamicFeature.class,
            JaxRsRequestInterceptor.class // Custom interceptor type
    );


    private static final Logger LOGGER = LoggerFactory.getLogger(JaxrsWhiteboard.class);
    public static final int FAILURE_REASON_CONTEXT_NOT_AVAILABLE = 100;

    // Injected by dependency manager
    private volatile DependencyManager m_dependencyManager;

    private final List<ApplicationServiceImpl> applicationServices = new ArrayList<>();
    private final SortedMap<ServiceReference<?>, ApplicationHolder> m_applicationHolderMap = new ConcurrentSkipListMap<>(Comparator.reverseOrder());
    private final Map<ServiceReference<?>, ResourceHolder> m_resourceHolderMap = new HashMap<>();
    private final Map<ServiceReference<?>, ExtensionHolder> m_extensionHolderMap = new HashMap<>();
    private final SortedSet<ServiceReference<ServletContextHelper>> m_servletContextReferences = new TreeSet<>(Comparator.reverseOrder());
    private final Object m_lock = new Object();

    private final Map<ServiceReference<?>, FailedApplicationDTO> m_failedApplicationDTOs = new HashMap<>();
    private final Map<ServiceReference<?>, FailedResourceDTO> m_failedResourceDTOs = new HashMap<>();
    private final Map<ServiceReference<?>, FailedExtensionDTO> m_failedExtensionDTOs = new HashMap<>();

    private final JaxrsServiceRuntimeImpl m_jaxrsServiceRuntime = new JaxrsServiceRuntimeImpl();
    private volatile Component m_jaxrsServiceRuntimeComponent;

    @SuppressWarnings("unused" /* dm callback method */)
    protected final void start() {
        m_jaxrsServiceRuntimeComponent = m_dependencyManager.createComponent()
                .setInterface(JaxrsServiceRuntime.class.getName(), null)
                .setImplementation(m_jaxrsServiceRuntime);
        m_dependencyManager.add(m_jaxrsServiceRuntimeComponent);
    }

    @SuppressWarnings("unused" /* dm callback method */)
    protected final void stop() {
        m_dependencyManager.remove(m_jaxrsServiceRuntimeComponent);
    }

    /**
     * Dependency Manager callback that is called when a new {@link Application} has been registered in the service registry.
     * <p>
     * Fisrt check if the {@link Application} should be processed by this whiteboard it's validated. If not the
     * {@link Application} will be ignored.
     * <p>
     * If the application does target this this whiteboard it's first validated before actually adding it.
     * <p>
     * A valid application at least has:
     * <ul>
     * <li>{@link JaxrsWhiteboardConstants#JAX_RS_APPLICATION_BASE} is present and valid</li>
     * <li>{@link JaxrsWhiteboardConstants#JAX_RS_EXTENSION_SELECT} if present only contains valid {@link Filter} values</li>
     * <li>{@link AmdatuWebRestConstants#JAX_RS_APPLICATION_CONTEXT} if present contains a valid context name</li>
     * </ul>
     * <p>
     * If the {@link Application} is invalid a {@link FailedApplicationDTO} is created and after that the {@link Application}
     * is ignored by the whiteboard.
     * <p>
     * If the {@link Application} is valid it will be added to the whiteboard known resources and extensions that have a
     * matching {@link JaxrsWhiteboardConstants#JAX_RS_APPLICATION_SELECT} filter will be linked to the application.
     * <p>
     * After that all applications will be re-evaluated as it's possible that this newly added application is shadowing
     * an application that is already registered.
     * After this re-evaluation the application will either be available OR an {@link FailedApplicationDTO} has been added
     * to the {@link RuntimeDTO}
     *
     * @param serviceReference {@link ServiceReference} of the {@link Application}
     * @param application      The {@link Application} instance
     */
    @SuppressWarnings("unused" /* dm callback method */)
    protected final void applicationAdded(ServiceReference<Application> serviceReference, Application application) {

        String jaxRsApplicationContext = AmdatuWebRestConstants.JAX_RS_APPLICATION_CONTEXT;
        ApplicationHolder applicationHolder;
        try {
            if (!isTargetingWhiteboard(serviceReference)) {
                // Service is not targeting this whiteboard
                return;
            }

            Set<Filter> extensionSelect = getExtensionSelect(serviceReference);
            String applicationBaseUri = getApplicationBaseUri(serviceReference);
            String servletContextName = getServletContextName(serviceReference);

            applicationHolder = new ApplicationHolder(serviceReference, application, extensionSelect, applicationBaseUri, servletContextName);
        }
        catch (InvalidWhiteboardServiceException e) {
            LOGGER.error("Invalid application", e);

            FailedApplicationDTO failedApplicationDTO = createFailedApplicationDTO(serviceReference, DTOConstants.FAILURE_REASON_VALIDATION_FAILED);
            m_failedApplicationDTOs.put(serviceReference, failedApplicationDTO);
            return;
        }

        synchronized (m_lock) {
            LOGGER.debug("Application added: {}", applicationHolder.getName());
            m_applicationHolderMap.put(serviceReference, applicationHolder);
            m_resourceHolderMap.values().stream()
                    .filter(resource -> resource.getApplicationSelect().match(applicationHolder.getServiceReference()))
                    .forEach(applicationHolder::addWhiteboardResource);

            m_extensionHolderMap.values().stream()
                    .filter(extension -> extension.getApplicationSelect().match(applicationHolder.getServiceReference()))
                    .forEach(applicationHolder::addWhiteboardExtension);

            updateExternalState();
        }
    }

    @SuppressWarnings("unused" /* dm callback method */)
    protected final void applicationChanged(ServiceReference<Application> serviceReference, Application application) {
        applicationRemoved(serviceReference, application);
        applicationAdded(serviceReference, application);
    }

    @SuppressWarnings("unused" /* dm callback method */)
    protected final void applicationRemoved(ServiceReference<Application> serviceReference, Application application) {
        try {
            if (!isTargetingWhiteboard(serviceReference)) {
                // Service is not targeting this whiteboard
                return;
            }
        }
        catch (InvalidWhiteboardServiceException e) {
            m_failedApplicationDTOs.remove(serviceReference);
        }

        synchronized (m_lock) {
            ApplicationHolder applicationHolder = m_applicationHolderMap.remove(serviceReference);
            LOGGER.debug("Application removed: ()", applicationHolder.getName());
            updateExternalState();
        }
    }

    /**
     * Dependency Manager callback that is called when a new service with a {@link JaxrsWhiteboardConstants#JAX_RS_RESOURCE}
     * service property has been registered in the service registry.
     *
     * <p>
     * Fisrt check if the resource should be processed by this whiteboard it's validated. If not the resource will be ignored.
     * <p>
     * If the resource does target this this whiteboard it's first validated before actually adding it.
     * <p>
     * A valid resource at least has:
     * <ul>
     * <li>{@link JaxrsWhiteboardConstants#JAX_RS_EXTENSION_SELECT} if present only contains valid {@link Filter} values</li>
     * <li>{@link AmdatuWebRestConstants#JAX_RS_APPLICATION_CONTEXT} if present contains a valid context name</li>
     * </ul>
     *
     * <p>
     * If the resource is <b>invalid</b> a {@link FailedResourceDTO} is created and after that the resource is ignored by the whiteboard.
     * </p>
     * <p>
     * If the resource is <b>valid</b> it will be added to the whiteboard and linked to all currently known aplications
     * matching the resource {@link JaxrsWhiteboardConstants#JAX_RS_APPLICATION_SELECT} filter.
     * </p>
     *
     * @param serviceReference {@link ServiceReference} for the resource service
     * @param service          resource service instance
     */
    @SuppressWarnings("unused" /* dm callback method */)
    protected final void resourceAdded(ServiceReference<Object> serviceReference, Object service) {
        ResourceHolder resourceHolder;
        try {
            if (!isTargetingWhiteboard(serviceReference)) {
                // Service is not targeting this whiteboard
                return;
            }
            Set<Filter> extensionSelect = getExtensionSelect(serviceReference);
            Filter applicationSelect = getApplicationSelect(serviceReference);
            resourceHolder = new ResourceHolder(serviceReference, service, extensionSelect, applicationSelect);
        }
        catch (InvalidWhiteboardServiceException e) {
            LOGGER.error("Invalid resource", e);
            FailedResourceDTO failedResourceDTO = createFailedResourceDTO(serviceReference, DTOConstants.FAILURE_REASON_VALIDATION_FAILED);

            m_failedResourceDTOs.put(serviceReference, failedResourceDTO);
            return;
        }

        synchronized (m_lock) {
            m_resourceHolderMap.put(serviceReference, resourceHolder);

            // add extension to matching applications
            m_applicationHolderMap.values()
                    .stream()
                    .filter(application -> resourceHolder.getApplicationSelect().match(application.getServiceReference()))
                    .forEach(application -> {
                        application.addWhiteboardResource(resourceHolder);
                        LOGGER.debug("Resource '{}' added to application '{}'", resourceHolder.getName(), application.getName());
                    });

            updateExternalState();
        }
    }

    @SuppressWarnings("unused" /* dm callback method */)
    protected final void resourceChanged(ServiceReference<Object> serviceReference, Object service) {
        resourceRemoved(serviceReference, service);
        resourceAdded(serviceReference, service);
    }

    @SuppressWarnings("unused" /* dm callback method */)
    protected final void resourceRemoved(ServiceReference<Object> serviceReference, Object service) {
        try {
            if (!isTargetingWhiteboard(serviceReference)) {
                // Service is not targeting this whiteboard
                return;
            }
        }
        catch (InvalidWhiteboardServiceException e) {
            m_failedResourceDTOs.remove(serviceReference);
        }

        synchronized (m_lock) {
            ResourceHolder resourceHolder = m_resourceHolderMap.remove(serviceReference);

            // Remove the resource from applications
            m_applicationHolderMap.values()
                    .stream()
                    .filter(application -> resourceHolder.getApplicationSelect().match(application.getServiceReference()))
                    .forEach(application -> {
                        application.removeWhiteboardResource(resourceHolder);
                        LOGGER.debug("Resource '{}' removed from application '{}'", resourceHolder.getName(), application.getName());
                    });

            updateExternalState();
        }
    }

    /**
     * Dependency Manager callback that is called when a new service with a {@link JaxrsWhiteboardConstants#JAX_RS_EXTENSION}
     * service property has been registered in the service registry.
     *
     * <p>
     * Fisrt check if the extension should be processed by this whiteboard it's validated. If not the extension will be ignored.
     * <p>
     * If the extension does target this this whiteboard it's first validated before actually adding it.
     * <p>
     * A valid extension at least has:
     * <ul>
     * <li>{@link JaxrsWhiteboardConstants#JAX_RS_EXTENSION_SELECT} if present only contains valid {@link Filter} values</li>
     * <li>{@link AmdatuWebRestConstants#JAX_RS_APPLICATION_CONTEXT} if present contains a valid context name</li>
     * <li>{@link Constants#OBJECTCLASS} That contains at least one JAX-RS extension contract type</li>
     * </ul>
     *
     * <p>
     * If the extension is <b>invalid</b> a {@link FailedExtensionDTO} is created and after that the extension is ignored by the whiteboard.
     * </p>
     * <p>
     * If the extension is <b>valid</b> it will be added to the whiteboard and linked to all currently known aplications
     * matching the resource {@link JaxrsWhiteboardConstants#JAX_RS_APPLICATION_SELECT} filter.
     * </p>
     *
     * @param serviceReference {@link ServiceReference} for the extension service
     * @param service          extension service instance
     */
    @SuppressWarnings("unused" /* dm callback method */)
    protected final void extensionAdded(ServiceReference<Object> serviceReference, Object service) {

        ExtensionHolder extensionHolder;
        try {
            if (!isTargetingWhiteboard(serviceReference)) {
                // Service is not targeting this whiteboard
                return;
            }

            Set<Filter> extensionSelect = getExtensionSelect(serviceReference);
            Filter applicationSelect = getApplicationSelect(serviceReference);
            Set<Class<?>> contracts = getContracts(serviceReference);


            extensionHolder = new ExtensionHolder(serviceReference, service, extensionSelect, applicationSelect, contracts);
        }
        catch (InvalidWhiteboardServiceException e) {
            FailedExtensionDTO failedExtensionDTO = createFailedExtensionDTO(serviceReference, DTOConstants.FAILURE_REASON_VALIDATION_FAILED);
            m_failedExtensionDTOs.put(serviceReference, failedExtensionDTO);
            LOGGER.error("Invalid extension", e);
            return;
        }

        synchronized (m_lock) {
            m_extensionHolderMap.put(serviceReference, extensionHolder);

            // add extension to matching applications
            m_applicationHolderMap.values()
                    .stream()
                    .filter(application -> extensionHolder.getApplicationSelect().match(application.getServiceReference()))
                    .forEach(application -> {
                        application.addWhiteboardExtension(extensionHolder);
                        LOGGER.debug("Extension '{}' added to application '{}'", extensionHolder.getName(), application.getName());
                    });

            // updateExternalState register applications that were not yet registered due to missing extensions
            updateExternalState();
        }

    }


    @SuppressWarnings("unused" /* dm callback method */)
    protected final void extensionChanged(ServiceReference<Object> serviceReference, Object service) {
        extensionRemoved(serviceReference, service);
        extensionAdded(serviceReference, service);
    }

    @SuppressWarnings("unused" /* dm callback method */)
    protected final void extensionRemoved(ServiceReference<Object> serviceReference, Object service) {
        try {
            if (!isTargetingWhiteboard(serviceReference)) {
                // Service is not targeting this whiteboard
                return;
            }
        }
        catch (InvalidWhiteboardServiceException e) {
            m_failedExtensionDTOs.remove(serviceReference);
            return; // The target filter is invalid
        }

        synchronized (m_lock) {
            ExtensionHolder extensionHolder = m_extensionHolderMap.remove(serviceReference);

            // add extension from applications
            m_applicationHolderMap.values()
                    .stream()
                    .filter(application -> extensionHolder.getApplicationSelect().match(application.getServiceReference()))
                    .forEach(application -> {
                        application.removeWhiteboardExtension(extensionHolder);
                        LOGGER.debug("Extension '{}' removed from application '{}'", extensionHolder.getName(), application.getName());
                    });

            // updateExternalState will unregister a
            updateExternalState();
        }
    }

    /**
     * Dependency Manager callback that is called when a new {@link ServletContextHelper} has been added to the service
     * registry.
     * <p>
     * The witeboard keeps track of all available servlet contexts to be able to detect if an application is shadowing
     * another application. As this whiteboard allows to specify servlet context that should be used to register the rest
     * endpoints it's the application path must be prefixed with the context path to determine if an applidacation is
     * shadowing another application.
     *
     * @param ref {@link ServiceReference} of the added {@link ServletContextHelper}
     */
    @SuppressWarnings("unused" /* dm callback method */)
    protected final void servletContextAdded(ServiceReference<ServletContextHelper> ref) {
        synchronized (m_lock) {
            m_servletContextReferences.add(ref);
            updateExternalState();
        }
    }

    @SuppressWarnings("unused" /* dm callback method */)
    protected final void servletContextChanged(ServiceReference<ServletContextHelper> ref) {
        synchronized (m_lock) {
            updateExternalState();
        }
    }

    @SuppressWarnings("unused" /* dm callback method */)
    protected final void servletContextRemoved(ServiceReference<ServletContextHelper> ref) {
        boolean removed = m_servletContextReferences.remove(ref);

        if (removed) {
            synchronized (m_lock) {
                updateExternalState();
            }
        }
    }

    /**
     * Update the extenally visible state.
     * <p>
     * The externally visible state consists of
     * <ul>
     * <li>A registered ApplicationService for each valid {@link Application}</li>
     * <li>The {@link RuntimeDTO}</li>
     * </ul>
     * <p>
     * This method will:
     * Determine if there are any applications that should not be published because they are in a failure state. For
     * Failed applications a {@link FailedApplicationDTO} will be created.
     * <p>
     * Remove ApplicationService components that were prviously registerd for {@link ApplicationHolder} that are
     * removed or invalid after the changes in the whiteboard.
     * <p>
     * Register ApplicationService components for {@link ApplicationHolder} that were previously unavailable or in a
     * failed state but are now ok.
     * <p>
     * Create an updated {@link RuntimeDTO} reflecting the current state of the whiteboard.
     */
    private void updateExternalState() {
        LOGGER.debug("Updating external state");
        Map<ApplicationHolder, FailedApplicationDTO> failedApplications = determineFailedApplications();

        // Update Application services

        updateApplicationServices(failedApplications);
        updateRuntimeDTO(failedApplications);

        LOGGER.debug("External state updated");
    }

    private void updateApplicationServices(Map<ApplicationHolder, FailedApplicationDTO> failedApplications) {
        Iterator<ApplicationServiceImpl> applicationServiceIterator = applicationServices.iterator();
        while (applicationServiceIterator.hasNext()) {
            ApplicationServiceImpl applicationService = applicationServiceIterator.next();
            ApplicationHolder applicationHolder = applicationService.getApplicationHolder();

            if (failedApplications.containsKey(applicationHolder)) {
                LOGGER.info("Unregistering ApplicationService for failed application {}", applicationHolder.getName());
                m_dependencyManager.remove(applicationService.getComponent());
                applicationServiceIterator.remove();
            }
            else if (!m_applicationHolderMap.containsValue(applicationHolder)) {
                LOGGER.info("Unregistering ApplicationService for removed application {}", applicationHolder.getName());
                m_dependencyManager.remove(applicationService.getComponent());
                applicationServiceIterator.remove();
            }
        }

        List<ApplicationHolder> toBeRegistered = new ArrayList<>(m_applicationHolderMap.values());
        toBeRegistered.removeAll(failedApplications.keySet());
        for (ApplicationServiceImpl applicationService : applicationServices) {
            applicationService.update();
            toBeRegistered.remove(applicationService.getApplicationHolder());
        }

        for (ApplicationHolder applicationHolder : toBeRegistered) {
            LOGGER.info("Registering ApplicationService for application {}", applicationHolder.getName());
            ApplicationServiceImpl applicationService = new ApplicationServiceImpl(applicationHolder);

            Component component = m_dependencyManager.createComponent()
                    .setInterface(ApplicationService.class.getName(), null)
                    .setImplementation(applicationService);
            m_dependencyManager.add(component);
            applicationServices.add(applicationService);
        }
    }

    private void updateRuntimeDTO(Map<ApplicationHolder, FailedApplicationDTO> failedApplications) {
        RuntimeDTO runtimeDTO = new RuntimeDTO();

        List<FailedApplicationDTO> failedApplicationDTOS = new ArrayList<>(m_failedApplicationDTOs.values());
        failedApplicationDTOS.addAll(failedApplications.values());

        runtimeDTO.applicationDTOs = applicationServices.stream()
                .map(applicationService -> createApplicationDTO(applicationService.getApplicationHolder()))
                .toArray(ApplicationDTO[]::new);

        runtimeDTO.failedApplicationDTOs = failedApplicationDTOS.toArray(new FailedApplicationDTO[0]);
        runtimeDTO.failedResourceDTOs = m_failedResourceDTOs.values().toArray(new FailedResourceDTO[0]);
        runtimeDTO.failedExtensionDTOs = m_failedExtensionDTOs.values().toArray(new FailedExtensionDTO[0]);

        // TODO: The ApplicationDTO has no failed resource / extension information that feels a bit odd as it's possible to have missing required extensions on an application level

        m_jaxrsServiceRuntime.setRuntimeDTO(runtimeDTO);
    }

    /**
     * Determine failure state for all applications.
     *
     * @return Map containing all applications that should not be available for some reason
     */
    private Map<ApplicationHolder, FailedApplicationDTO> determineFailedApplications() {
        Map<ApplicationHolder, FailedApplicationDTO> failedApplications = new HashMap<>();

        Set<String> paths = new HashSet<>();
        Set<String> names = new HashSet<>();
        for (ApplicationHolder service : m_applicationHolderMap.values()) {

            String contextPath = getApplicationContextPath(service);
            if (contextPath == null) {
                // if null the context is not available
                failedApplications.put(service, createFailedApplicationDTO(service.getServiceReference(), FAILURE_REASON_CONTEXT_NOT_AVAILABLE));
                continue;
            }

            String baseUriWithContext = contextPath + service.getBaseUri();

            if (paths.contains(baseUriWithContext)) {
                failedApplications.put(service, createFailedApplicationDTO(service.getServiceReference(), DTOConstants.FAILURE_REASON_SHADOWED_BY_OTHER_SERVICE));
            }
            else if (service.getName() != null && names.contains(service.getName())) {
                failedApplications.put(service, createFailedApplicationDTO(service.getServiceReference(), DTOConstants.FAILURE_REASON_SHADOWED_BY_OTHER_SERVICE));
            }
            else {
                paths.add(baseUriWithContext);
                if (service.getName() != null) {
                    names.add(service.getName());
                }
            }

            if (!service.requiredExtentionsAvailable()) {
                failedApplications.put(service, createFailedApplicationDTO(service.getServiceReference(), DTOConstants.FAILURE_REASON_REQUIRED_EXTENSIONS_UNAVAILABLE));
            }
        }
        return failedApplications;
    }

    /**
     * Check if a {@link AbstractHolder} should be processed by this whiteboard based on it's {@link ServiceReference}.
     *
     * @param serviceReference {@link ServiceReference} of the service to check
     * @return <code>true</code> If the service should be processed
     * <br>
     * <code>false</code> If the service should not be processed
     * @throws InvalidWhiteboardServiceException if the value of the {@link JaxrsWhiteboardConstants#JAX_RS_WHITEBOARD_TARGET}
     *                                           is present but not a valid {@link Filter}
     * @see JaxrsWhiteboardConstants#JAX_RS_WHITEBOARD_TARGET
     */
    private boolean isTargetingWhiteboard(ServiceReference<?> serviceReference) {
        Object property = serviceReference.getProperty(JaxrsWhiteboardConstants.JAX_RS_WHITEBOARD_TARGET);

        if (property == null) {
            return true;
        }
        else {
            try {
                return createFilter((String) property).match(m_jaxrsServiceRuntimeComponent.getServiceProperties());
            }
            catch (InvalidSyntaxException e) {
                throw new InvalidWhiteboardServiceException(e);
            }
        }
    }

    /**
     * Get the base uri for an application from the service properties it was registered with.
     *
     * @param serviceReference {@link ServiceReference} of the {@link Application}
     * @return The base uri
     * @throws InvalidWhiteboardServiceException if the {@link JaxrsWhiteboardConstants#JAX_RS_APPLICATION_BASE}
     *                                           is missing or not a {@link String}
     * @see JaxrsWhiteboardConstants#JAX_RS_APPLICATION_BASE
     */
    private String getApplicationBaseUri(ServiceReference<Application> serviceReference) {
        Object applicationBase = serviceReference.getProperty(JAX_RS_APPLICATION_BASE);

        if (!(applicationBase instanceof String)) {
            throw new InvalidWhiteboardServiceException("Application base property is required and must be of type String");
        }

        String base = (String) applicationBase;
        // Make sure base starts with a '/' (spec)
        if (!base.startsWith("/")) {
            base = "/" + base;
        }

        // Strip trailing '/' (this makes shadowing detection more predictable)
        if (base.endsWith("/")) {
            base = base.substring(0, base.length() - 1);
        }
        return base;
    }

    /**
     * Get the name of the servlet context that should be used from the service properties the application was
     * registered with.
     * <p>
     * This is an Amdatu Web feature and not part of the OSGi JAX-RS Whiteboard spec.
     * </p>
     *
     * @param serviceReference {@link ServiceReference} of the {@link Application}
     * @return Name of the context that should be used, {@link HttpWhiteboardConstants#HTTP_WHITEBOARD_DEFAULT_CONTEXT_NAME}
     * if no name was specified <code>never null</code>
     * @throws InvalidWhiteboardServiceException If the value for the {@link AmdatuWebRestConstants#JAX_RS_APPLICATION_CONTEXT}
*                                                service property is not of type {@link String}
     * @see org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants#JAX_RS_APPLICATION_CONTEXT
     */
    private String getServletContextName(ServiceReference<Application> serviceReference) {
        Object contextName = serviceReference.getProperty(JAX_RS_APPLICATION_CONTEXT);

        if (contextName == null) {
            contextName = HttpWhiteboardConstants.HTTP_WHITEBOARD_DEFAULT_CONTEXT_NAME;
        }

        if (!(contextName instanceof String)) {
            throw new InvalidWhiteboardServiceException("Application context property is must be of type String");
        }

        return (String) contextName;
    }

    /**
     * Get the context path for a {@link ApplicationHolder}.
     *
     * @param service The {@link ApplicationHolder} to get the context path for
     * @return the context path
     * <code>null</code> if there is no {@link ServletContextHelper} registered with the name from {@link ApplicationHolder#getServletContextName()}
     */
    private String getApplicationContextPath(ApplicationHolder service) {
        String contextName = service.getServletContextName();
        Optional<ServiceReference<ServletContextHelper>> servletContextHelperServiceReference = m_servletContextReferences.stream()
                .filter(ref -> contextName.equals(ref.getProperty(HTTP_WHITEBOARD_CONTEXT_NAME)))
                .findFirst();

        String contextPath;
        if (servletContextHelperServiceReference.isPresent()) {
            contextPath = (String) servletContextHelperServiceReference.get().getProperty(HTTP_WHITEBOARD_CONTEXT_PATH);

            if (!contextPath.startsWith("/")) {
                contextPath = "/" + contextPath;
            }

            if (contextPath.endsWith("/")) {
                contextPath = contextPath.substring(0, contextPath.length() - 1);
            }
        }
        else {
            if (HttpWhiteboardConstants.HTTP_WHITEBOARD_DEFAULT_CONTEXT_NAME.equals(contextName)) {
                contextPath = ""; // when using the default context we don't need a prefix.
            }
            else {
                contextPath = null;
            }
        }
        return contextPath;
    }

    /**
     * Get the application select filter for a  {@link AbstractHolder} from it's {@link ServiceReference}.
     *
     * @param serviceReference {@link ServiceReference} of the {@link AbstractHolder}
     * @return The application select filter for this {@link AbstractHolder}, when no {@link JaxrsWhiteboardConstants#JAX_RS_APPLICATION_SELECT}
     * service property is present a default filter is returned <code>never null</code>
     * @throws InvalidWhiteboardServiceException if the value of the {@link JaxrsWhiteboardConstants#JAX_RS_APPLICATION_SELECT}
     *                                           is present but not a valid {@link Filter}
     */
    private Filter getApplicationSelect(ServiceReference<Object> serviceReference) {
        final Filter applicationSelectFilter;

        Object applicationSelect = serviceReference.getProperty(JAX_RS_APPLICATION_SELECT);
        String filterString;

        if (applicationSelect == null) {
            filterString = DEFAULT_APPLICATION_SELECT;
        } else if (applicationSelect instanceof String) {
            String applicationSelectString = (String) applicationSelect;
            if (JaxrsWhiteboardUtils.isValidSymbolicName(applicationSelectString)) {
                filterString = String.format("(%s=%s)", JaxrsWhiteboardConstants.JAX_RS_NAME, applicationSelectString);
            } else {
                filterString = applicationSelectString;
            }
        } else {
            throw new InvalidWhiteboardServiceException(format("Invalid %s property, expected String but was %s", JAX_RS_APPLICATION_SELECT, applicationSelect.getClass()));
        }

        try {
            applicationSelectFilter = FrameworkUtil.createFilter(filterString);
        } catch (InvalidSyntaxException e) {
            throw new InvalidWhiteboardServiceException(e);
        }

        return applicationSelectFilter;
    }

    /**
     * Get the application select filter for a  {@link AbstractHolder} from it's {@link ServiceReference}.
     *
     * @param serviceReference {@link ServiceReference} of the {@link AbstractHolder}
     * @return The application select filter for this {@link AbstractHolder}, when no {@link JaxrsWhiteboardConstants#JAX_RS_APPLICATION_SELECT}
     * service property is present a default filter is returned <code>never null</code>
     * @throws InvalidWhiteboardServiceException if the value of the {@link JaxrsWhiteboardConstants#JAX_RS_APPLICATION_SELECT}
     *                                           is present but not a valid {@link Filter}
     */
    private Set<Filter> getExtensionSelect(ServiceReference<?> serviceReference) {
        try {
            return getExtensionSelectFilters(serviceReference);
        }
        catch (InvalidSyntaxException e) {
            throw new InvalidWhiteboardServiceException(e);
        }
    }

    /**
     * Get the contracts for a {@link ExtensionHolder} from it's {@link ServiceReference}.
     *
     * @param serviceReference {@link ServiceReference} of the {@link ExtensionHolder}
     * @return Set of contract classes the the {@link ExtensionHolder} is registered with in the service registry.
     * @throws InvalidWhiteboardServiceException when there are no contracts
     */
    private Set<Class<?>> getContracts(ServiceReference<Object> serviceReference) throws InvalidWhiteboardServiceException {
        List<String> objectClass = Arrays.asList((String[]) serviceReference.getProperty(Constants.OBJECTCLASS));
        Set<Class<?>> contracts = SUPPORTED_EXTENSION_CONTRACTS.stream()
                .filter(contractClass -> objectClass.contains(contractClass.getName()))
                .collect(Collectors.toSet());

        if (contracts.isEmpty()) {
            throw new InvalidWhiteboardServiceException("No contracts ");
        }

        return contracts;
    }
}
