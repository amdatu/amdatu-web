/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.whiteboard;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.core.Application;

import org.osgi.framework.Filter;
import org.osgi.framework.ServiceReference;

public class ApplicationHolder extends AbstractHolder {

    private final Application m_application;
    private final String m_baseUri;
    private final String m_servletContextName;

    private Set<ExtensionHolder> m_extensionHolders = new HashSet<>();
    private Set<ResourceHolder> m_resourceHolders = new HashSet<>();

    public ApplicationHolder(ServiceReference<?> serviceReference, Application service, Set<Filter> extensionSelect, String baseUri, String servletContextName) {
        super(serviceReference, service, extensionSelect);
        m_application = service;
        m_baseUri = baseUri;
        m_servletContextName = servletContextName;
    }

    public Application getApplication() {
        return m_application;
    }

    public String getBaseUri() {
        return m_baseUri;
    }

    public String getServletContextName() {
        return m_servletContextName;
    }

    public void addWhiteboardResource(ResourceHolder resourceHolder) {
        m_resourceHolders.add(resourceHolder);
    }

    public void removeWhiteboardResource(ResourceHolder resourceHolder) {
        m_resourceHolders.remove(resourceHolder);
    }

    public void addWhiteboardExtension(ExtensionHolder extensionHolder) {
        m_extensionHolders.add(extensionHolder);
    }

    public void removeWhiteboardExtension(ExtensionHolder extensionHolder) {
        m_extensionHolders.remove(extensionHolder);
    }

    public List<ResourceHolder> getResources() {
        return m_resourceHolders.stream()
                .filter(this::hasRequiredExtensionsFor)
                .sorted(Comparator.comparing(AbstractHolder::getServiceReference).reversed())
                .collect(Collectors.toList());
    }

    public List<ExtensionHolder> getExtensions() {
        return m_extensionHolders.stream()
                .filter(this::hasRequiredExtensionsFor)
                .sorted(Comparator.comparing(AbstractHolder::getServiceReference).reversed())
                .collect(Collectors.toList());
    }

    /**
     * Check if all required dependencies for this application are available
     *
     * @return <code>true</code> the resource or extension has no required extensions or all extensions are available.
     * <code>false</code> if a required extension is not available
     */
    public boolean requiredExtentionsAvailable() {
        return hasRequiredExtensionsFor(this);
    }

    /**
     * Check if this Application has all the required extensions available for a Resource or Extension
     *
     * @param holder {@link ResourceHolder} or {@link ExtensionHolder}
     * @return <code>true</code> the resource or extension has no required extensions or all extensions are available.
     * <code>false</code> if a required extension is not available
     */
    public boolean hasRequiredExtensionsFor(AbstractHolder holder) {
        return hasRequiredExtensionsFor(holder, Collections.emptyList());
    }

    private boolean hasRequiredExtensionsFor(AbstractHolder holder, List<ExtensionHolder> dependencyChain) {
        for (Filter filter : holder.getExtensionSelect()) {
            boolean extensionAvailable = m_extensionHolders.stream()
                    .filter(extensionService -> filter.match(extensionService.getServiceReference()))
                    .filter(service -> !dependencyChain.contains(service)) // Prevent inifinite loop
                    .anyMatch(service -> {
                        List<ExtensionHolder> chain = new ArrayList<>(dependencyChain);
                        chain.add(service);
                        return hasRequiredExtensionsFor(service, chain);
                    });

            if (!extensionAvailable) {
                return false;
            }
        }
        return true;
    }
}
