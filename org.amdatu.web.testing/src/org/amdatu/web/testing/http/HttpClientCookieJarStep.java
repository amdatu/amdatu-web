/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.testing.http;

import java.net.CookieHandler;

import org.amdatu.testing.configurator.ConfigurationStep;
import org.amdatu.web.testing.http.util.MemoryCookieJar;

/**
 * Provides a configuration step that allows HttpURLConnection to make use of persistent cookies for the duration of the test suite.
 */
public class HttpClientCookieJarStep implements ConfigurationStep {
    private final MemoryCookieJar m_cookieJar;
    private CookieHandler m_oldCookieHandler;

    public HttpClientCookieJarStep() {
        m_cookieJar = new MemoryCookieJar();
    }

    @Override
    public void apply(Object testCase) {
        // By default, no cookie handler is present, but if some other
        // library has injected one, we'd rather put it back at the end...
        m_oldCookieHandler = CookieHandler.getDefault();
        CookieHandler.setDefault(m_cookieJar);
    }

    @Override
    public void cleanUp(Object testCase) {
        CookieHandler.setDefault(m_oldCookieHandler);
    }
}
