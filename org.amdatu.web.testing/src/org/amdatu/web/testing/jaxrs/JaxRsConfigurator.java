/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.testing.jaxrs;

import org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

/**
 * Provides factory methods for {@link JaxRsConfigurationStep}s
 */
public class JaxRsConfigurator {

    private JaxRsConfigurator() {
        // Not for instantiation.
    }


    /**
     * Creates a new {@link JaxRsConfigurationStep} configured with the given parameters and the following defaults:
     * <ol>
     * <li>an application name of "<tt>IntegrationTest</tt>";
     * <li>a JSON provider as registered singleton (JSON requests/responses are supported).
     * </ol>
     *
     * @param base the base path to use for the registered JAXRS resources, cannot be <code>null</code>.
     * @return the configured JAXRS configuration step, never <code>null</code>.
     */
    public static JaxRsConfigurationStep createJaxRsApplication(String base) {
        return createJaxRsApplication("IntegrationTest" , base);
    }

    /**
     * Creates a new {@link JaxRsConfigurationStep} configured with the given parameters and the following defaults:
     * <ol>
     * <li>a JSON provider as registered singleton (JSON requests/responses are supported).
     * </ol>
     *
     * @param name the name of the application to use, cannot be <code>null</code>;
     * @param base the base path to use for the registered JAXRS resources, cannot be <code>null</code>.
     * @return the configured JAXRS configuration step, never <code>null</code>.
     */
    public static JaxRsConfigurationStep createJaxRsApplication(String name, String base) {
        return new JaxRsConfigurationStep()
            .setApplicationBase(base)
            .setApplicationName(name)
            .addSingletons(new JacksonJsonProvider());
    }
}
