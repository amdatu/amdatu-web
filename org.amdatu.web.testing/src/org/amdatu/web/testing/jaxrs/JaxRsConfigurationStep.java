/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.testing.jaxrs;

import static java.util.Arrays.asList;
import static org.amdatu.testing.configurator.TestConfigurator.createComponent;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_NAME;

import java.util.HashSet;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;

import javax.ws.rs.core.Application;

import org.amdatu.testing.configurator.ConfigurationStep;
import org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

/**
 * Provides a {@link ConfigurationStep} for creating JAXRS {@link Application}s to simplify testing of JAXRS resources.
 */
public class JaxRsConfigurationStep implements ConfigurationStep {

    private class TestApplication extends Application {
        @Override
        public Set<Class<?>> getClasses() {
            return m_classes;
        }

        @Override
        public Set<Object> getSingletons() {
            return m_singletons;
        }
    }

    private final Set<Object> m_singletons;
    private final Set<Class<?>> m_classes;
    private String m_applicationName;
    private String m_applicationBase;

    private Component m_component;

    public JaxRsConfigurationStep() {
        m_singletons = new HashSet<>();
        m_classes = new HashSet<>();
    }

    /**
     * Adds one or more types as part of this configured application, retaining any existing types.
     *
     * @param clazz the type to add as part of the configured application, cannot be <code>null</code>;
     * @param classes the optional types to add as part of the configured application.
     * @return this configuration step.
     */
    public JaxRsConfigurationStep addClasses(Class<?> clazz, Class<?>... classes) {
        if (clazz == null) {
            throw new IllegalArgumentException("At least one class should be given!");
        }
        m_classes.add(clazz);
        m_classes.addAll(asList(classes));
        return this;
    }

    /**
     * Adds one or more singletons to this configured application, retaining any existing singletons.
     *
     * @param singleton the singleton to add, cannot be <code>null</code>;
     * @param singletons the optional singletons to add.
     * @return this configuration step.
     */
    public JaxRsConfigurationStep addSingletons(Object singleton, Object... singletons) {
        if (singleton == null) {
            throw new IllegalArgumentException("At least one singleton should be given!");
        }
        m_singletons.add(singleton);
        m_singletons.addAll(asList(singletons));
        return this;
    }

    @Override
    public void apply(Object testCase) {
        Properties appProps = new Properties();
        appProps.put(JAX_RS_NAME, m_applicationName);
        appProps.put(JAX_RS_APPLICATION_BASE, m_applicationBase);

        m_component = createComponent()
            .setInterface(Application.class.getName(), appProps)
            .setImplementation(new TestApplication());

        getDM()
            .ifPresent(dm -> dm.add(m_component));
    }

    @Override
    public void cleanUp(Object testCase) {
        getDM()
            .ifPresent(dm -> dm.remove(m_component));

        m_component = null;
    }

    /**
     * @param applicationBase the application base path to set, that is used as prefix for all JAXRS endpoints.
     * @return this configuration step.
     */
    public JaxRsConfigurationStep setApplicationBase(String applicationBase) {
        if (applicationBase == null) {
            throw new IllegalArgumentException("Invalid application name!");
        }
        m_applicationBase = applicationBase;
        return this;
    }

    /**
     * @param applicationName the application name to set, to identify the group of registered JAXRS endpoints for this
     *        application.
     * @return this configuration step.
     */
    public JaxRsConfigurationStep setApplicationName(String applicationName) {
        if (applicationName == null) {
            throw new IllegalArgumentException("Invalid application name!");
        }
        m_applicationName = applicationName;
        return this;
    }

    /**
     * Replaces the types of this configured application with the types given.
     *
     * @param clazz the type to set as part of the configured application, cannot be <code>null</code>;
     * @param classes the optional types to set as part of the configured application.
     * @return this configuration step.
     */
    public JaxRsConfigurationStep setClasses(Class<?> clazz, Class<?>... classes) {
        m_classes.clear();
        return addClasses(clazz, classes);
    }

    /**
     * Replaces the singletons of this configured application with the singletons given.
     *
     * @param singleton the singleton to add, cannot be <code>null</code>;
     * @param singletons the optional singletons to add.
     * @return this configuration step.
     */
    public JaxRsConfigurationStep setSingletons(Object singleton, Object... singletons) {
        m_singletons.clear();
        return addSingletons(singleton, singletons);
    }

    private Optional<DependencyManager> getDM() {
        if (m_component == null) {
            return Optional.empty();
        }
        return Optional.ofNullable(m_component.getDependencyManager());
    }
}
