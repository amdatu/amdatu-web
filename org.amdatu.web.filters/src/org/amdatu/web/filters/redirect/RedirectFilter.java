/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.filters.redirect;

import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_FILTER_REGEX;

import java.io.IOException;
import java.util.Dictionary;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.cm.ConfigurationException;

/**
 * Redirects all requests that match the filter registration.
 * <p>
 * This filter is capable of using the (potential) capture groups of the filter-regex in the
 * redirect URL. It means that you can register a redirect filter like "/(\\w+)/?" and let it
 * redirect to "/$1/index.html" to have a kind of generic catch-all default page functionality.
 * </p>
 */
public class RedirectFilter implements Filter {

    private static final String REDIRECT_TO = "redirectTo";

    private volatile Pattern m_filterRegEx;
    private volatile String m_redirectTo;

    @Override
    public void destroy() {
        // do nothing
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        if (m_filterRegEx != null) {
            Matcher m = m_filterRegEx.matcher(request.getRequestURI());
            // Given that this filter is only invoked if the request URI actually
            // matches the regex, we can simply replace the first match directly...
            response.sendRedirect(m.replaceFirst(m_redirectTo));
        } else {
            response.sendRedirect(m_redirectTo);
        }
    }

    @Override
    public void init(FilterConfig cfg) throws ServletException {
        // do nothing
    }

    protected final void updated(Dictionary<String, ?> properties) throws ConfigurationException {
        String redirectTo = (String) properties.get(REDIRECT_TO);

        if (redirectTo == null || "".equals(redirectTo.trim())) {
            throw new ConfigurationException(REDIRECT_TO, "missing or empty configuration property!");
        }

        String filterRegEx = (String) properties.get(HTTP_WHITEBOARD_FILTER_REGEX);
        if (filterRegEx != null && !"".equals(filterRegEx.trim())) {
            m_filterRegEx = Pattern.compile(filterRegEx);
        }

        m_redirectTo = redirectTo;
    }
}
