/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.concurrency.itest.app2;

import java.util.Properties;

import javax.ws.rs.core.Application;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.log.LogService;

import static java.lang.Boolean.TRUE;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_RESOURCE;

public class Activator extends DependencyActivatorBase {
    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        Properties props = new Properties();
        props.put(Constants.SERVICE_RANKING, 20);

        manager.add(createComponent()
            .setInterface(Application.class.getName(), props)
            .setImplementation(ExampleApp2.class)
            .add(createServiceDependency().setService(LogService.class).setRequired(false)));


        Properties props2 = new Properties();
        props2.put(JAX_RS_RESOURCE, TRUE);
        manager.add(createComponent()
            .setInterface(Object.class.getName(), props2)
            .setImplementation(ExampleComponent.class)
            .add(createServiceDependency().setService(LogService.class).setRequired(false)));
    }

}