/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest;

import static java.lang.Boolean.TRUE;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_RESOURCE;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.osgi.framework.ServiceRegistration;

public class SubResourceTest extends RegistrationTestBase {

    @Path("root")
    public static class TestResource {
        @GET
        @Produces(MediaType.TEXT_PLAIN)
        public String handleGetRequest() {
            return "root";
        }
    }

    @Path("root")
    public static class TestResourceSubA {
        @GET
        @Path("a")
        @Produces(MediaType.TEXT_PLAIN)
        public String handleGetRequest() {
            return "sub-a";
        }
    }

    @Path("root")
    public static class TestResourceSubB {
        @GET
        @Path("b")
        @Produces(MediaType.TEXT_PLAIN)
        public String handleGetRequest() {
            return "sub-b";
        }
    }

    public void testSubResources() throws Exception {
        ServiceRegistration<Object> rootReg =
            registerService(Object.class, new TestResource(), JAX_RS_RESOURCE, TRUE);
        ServiceRegistration<Object> subAReg =
            registerService(Object.class, new TestResourceSubA(), JAX_RS_RESOURCE, TRUE);
        ServiceRegistration<Object> subBReg =
            registerService(Object.class, new TestResourceSubB(), JAX_RS_RESOURCE, TRUE);

        assertResponseContent("root", "rest/root");
        assertResponseContent("sub-a", "rest/root/a");
        assertResponseContent("sub-b", "rest/root/b");

        rootReg.unregister();
        assertResponseCode(HTTP_NOT_FOUND, "rest/root");
        assertResponseContent("sub-a", "rest/root/a");
        assertResponseContent("sub-b", "rest/root/b");

        subAReg.unregister();

        assertResponseCode(HTTP_NOT_FOUND, "rest/root");
        assertResponseCode(HTTP_NOT_FOUND, "rest/root/a");
        assertResponseContent("sub-b", "rest/root/b");

        subBReg.unregister();
        Thread.sleep(100l);
        assertResponseCode(HTTP_NOT_FOUND, "rest/root");
        assertResponseCode(HTTP_NOT_FOUND, "rest/root/a");
        assertResponseCode(HTTP_NOT_FOUND, "rest/root/b");
    }

}
