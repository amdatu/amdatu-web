/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest;

import org.osgi.framework.ServiceRegistration;

import javax.ws.rs.GET;
import javax.ws.rs.NameBinding;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Application;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.WriterInterceptor;
import javax.ws.rs.ext.WriterInterceptorContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Collections;
import java.util.Set;

import static java.lang.Boolean.TRUE;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_APPLICATION_SELECT;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_EXTENSION;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_RESOURCE;

/**
 * Tests the usage of {@link ContainerRequestFilter}
 */
public class InterceptorTest extends RegistrationTestBase {

    private static final String PREFIX = "/rest";
    private static final String RESOURCE = "/test";
    private static final String APP = "/app";
    private static final String JAX_RS_INTERCEPTOR_BASE = "TODO: REMOVE";

    @Path(RESOURCE)
    public static class TestResource {

        @GET
        public String handleGetRequest() {
            return "test";
        }
    }

    @Path(APP)
    public static class ApplicationResource {

        @GET
        public String handleGetRequest() {
            return "test";
        }
    }

    public static class TestApplication extends Application {

        @Override
        public Set<Object> getSingletons() {
            return Collections.singleton(new ApplicationResource());
        }

    }

    @Provider
    public static class TestWriterInterceptor implements WriterInterceptor {

        @Override
        public void aroundWriteTo(WriterInterceptorContext context) throws IOException, WebApplicationException {
            PrintWriter printWriter = new PrintWriter(context.getOutputStream());
            printWriter.write("[interceptor]");
            printWriter.flush();
            context.proceed();
        }

    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target( { ElementType.TYPE, ElementType.METHOD })
    @NameBinding
    public @interface NameBindingInterceptor { }

    @Provider
    @NameBindingInterceptor
    public static class TestNameBindingWriterInterceptor extends TestWriterInterceptor { }

    @Path(RESOURCE)
    @NameBindingInterceptor
    public static class TestResourceWithNameBindingInterceptorOnType extends TestResource {
    }

    @Path(RESOURCE)
    public static class TestResourceWithNameBindingInterceptorOnMethod {

        @GET
        @NameBindingInterceptor
        public String handleGetRequest() {
            return "test";
        }

        @GET
        @Path("other")
        public String handleOtherGetRequest() {
            return "test";
        }
    }

    @NameBindingInterceptor
    public static class TestApplicationWithNameBindingInterceptor extends Application {

        @Override
        public Set<Object> getSingletons() {
            return Collections.singleton(new ApplicationResource());
        }
    }

    public void testDefaultApplication() throws Exception {
        registerService(Object.class, new TestResource(), JAX_RS_RESOURCE, TRUE);
        assertResponseContent("test", PREFIX + RESOURCE);

        ServiceRegistration<WriterInterceptor> serviceRegistration = registerService(WriterInterceptor.class, new TestWriterInterceptor(), JAX_RS_EXTENSION, TRUE);

        assertResponseContent("[interceptor]test", PREFIX + RESOURCE);

        serviceRegistration.unregister();
        assertResponseContent("test", PREFIX + RESOURCE);
    }


    public void testCustomApplication() throws Exception {
        registerService(Application.class, new TestApplication(), JAX_RS_APPLICATION_BASE, APP);

        registerService(Object.class, new TestResource(),
                JAX_RS_RESOURCE, TRUE,
                JAX_RS_APPLICATION_SELECT, createTargetFilter(JAX_RS_APPLICATION_BASE, APP));

        assertResponseContent("test",  APP + RESOURCE);

        ServiceRegistration<WriterInterceptor> serviceRegistration = registerService(WriterInterceptor.class, new TestWriterInterceptor(),
                JAX_RS_EXTENSION, TRUE,
                JAX_RS_APPLICATION_SELECT, createTargetFilter(JAX_RS_APPLICATION_BASE, APP));

        assertResponseContent("[interceptor]test",  APP + RESOURCE);

        serviceRegistration.unregister();
        assertResponseContent("test",  APP + RESOURCE);
    }


    public void testNameBindingOnResource() throws Exception {
        registerService(Object.class, new TestResourceWithNameBindingInterceptorOnType(), JAX_RS_RESOURCE, TRUE);
        registerService(WriterInterceptor.class, new TestNameBindingWriterInterceptor(), JAX_RS_EXTENSION, TRUE);

        assertResponseContent("[interceptor]test",  PREFIX + RESOURCE);
    }

    public void testNameBindingOnMethod() throws Exception {
        registerService(Object.class, new TestResourceWithNameBindingInterceptorOnMethod(), JAX_RS_RESOURCE, TRUE);
        registerService(WriterInterceptor.class, new TestNameBindingWriterInterceptor(), JAX_RS_EXTENSION, TRUE);

        assertResponseContent("[interceptor]test",  PREFIX + RESOURCE);
        assertResponseContent("test",  PREFIX + RESOURCE + "/other");
    }

}
