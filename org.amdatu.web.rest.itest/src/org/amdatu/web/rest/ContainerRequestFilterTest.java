/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest;

import javax.ws.rs.GET;
import javax.ws.rs.NameBinding;
import javax.ws.rs.Path;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Collections;
import java.util.Set;

import static java.lang.Boolean.TRUE;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_APPLICATION_SELECT;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_EXTENSION;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_RESOURCE;

/**
 * Tests the usage of {@link ContainerRequestFilter}
 */
public class ContainerRequestFilterTest extends RegistrationTestBase {

    private static final String PREFIX = "/rest";

    private static final String RESOURCE = "/test";
    private static final String APP = "/app";

    @Path(RESOURCE)
    public static class TestResource {

        @GET
        public String handleGetRequest() {
            return "test";
        }
    }

    @Path(APP)
    public static class ApplicationResource {

        @GET
        public String handleGetRequest() {
            return "test";
        }
    }

    public static class TestApplication extends Application {

        @Override
        public Set<Object> getSingletons() {
            return Collections.singleton(new ApplicationResource());
        }

    }

    @Provider
    public static class TestContainerRequestFilter implements ContainerRequestFilter {

        private final Integer m_status;

        public TestContainerRequestFilter(int status) {
            m_status = status;
        }

        @Override
        public void filter(ContainerRequestContext requestContext) {
            requestContext.abortWith(Response.status(m_status).build());
        }

    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target( { ElementType.TYPE, ElementType.METHOD })
    @NameBinding
    public @interface NameBindingFilter { }

    @Provider
    @NameBindingFilter
    public static class TestNameBindingFilter extends TestContainerRequestFilter {
        public TestNameBindingFilter() {
            super(HTTP_FORBIDDEN);
        }
    }

    @Path(RESOURCE)
    @NameBindingFilter
    public static class TestResourceWithNameBindingFilterOnType extends TestResource {
    }

    @Path(RESOURCE)
    public static class TestResourceWithNameBindingFilterOnMethod {

        @GET
        @NameBindingFilter
        public String handleGetRequest() {
            return "test";
        }

        @GET
        @Path("other")
        public String handleOtherGetRequest() {
            return "test";
        }
    }

    @NameBindingFilter
    public static class TestApplicationWithNameBindingInterceptor extends Application {

        @Override
        public Set<Object> getSingletons() {
            return Collections.singleton(new ApplicationResource());
        }
    }

    public void testContainerRequestFilterDefaultApplication() throws Exception {
        registerService(Object.class, new TestResource(), JAX_RS_RESOURCE, TRUE);
        registerService(ContainerRequestFilter.class, new TestContainerRequestFilter(HTTP_FORBIDDEN), JAX_RS_EXTENSION, TRUE);

        assertResponseCode(HTTP_FORBIDDEN, PREFIX + RESOURCE);
    }

    public void testContainerRequestFilterCustomApplication() throws Exception {
        registerService(Application.class, new TestApplication(), JAX_RS_APPLICATION_BASE, APP);

        registerService(Object.class, new TestResource(),
                JAX_RS_RESOURCE, TRUE,
                JAX_RS_APPLICATION_SELECT, createTargetFilter(JAX_RS_APPLICATION_BASE, APP));

        registerService(ContainerRequestFilter.class, new TestContainerRequestFilter(HTTP_FORBIDDEN),
                JAX_RS_EXTENSION, TRUE,
                JAX_RS_APPLICATION_SELECT, createTargetFilter(JAX_RS_APPLICATION_BASE, APP));

        assertResponseCode(HTTP_FORBIDDEN, APP + RESOURCE);
    }


    public void testWhiteboardResourceNameBindingInterceptor() {
//        registerService(Object.class, new TestResourceWithNameBindingFilterOnType(), JAX_RS_RESOURCE_BASE, PREFIX);
//        registerService(Object.class, new TestNameBindingFilter(), JAX_RS_FILTER_BASE, PREFIX);
//
//        assertResponseCode(HTTP_FORBIDDEN, PREFIX + RESOURCE);
    }

    public void testWhiteboardResourceNameBindingInterceptor_method() {
//        registerService(Object.class, new TestResourceWithNameBindingFilterOnMethod(), JAX_RS_RESOURCE_BASE, PREFIX);
//        registerService(Object.class, new TestNameBindingFilter(), JAX_RS_FILTER_BASE, PREFIX);
//
//        assertResponseCode(HTTP_FORBIDDEN, PREFIX + RESOURCE);
//        assertResponseCode(HTTP_OK, PREFIX + RESOURCE + "/other");
    }

    public void testWhiteboardApplicationNameBindingInterceptor() {
//        registerService(Application.class, new TestApplicationWithNameBindingInterceptor(), JAX_RS_APPLICATION_BASE, PREFIX + "/intercept");
//        registerService(Application.class, new TestApplication(), JAX_RS_APPLICATION_BASE, PREFIX + "/no-intercept");
//        registerService(Object.class, new TestNameBindingFilter(), JAX_RS_FILTER_BASE, PREFIX);
//
//        assertResponseCode(HTTP_FORBIDDEN, PREFIX + "/intercept" + APP);
//        assertResponseCode(HTTP_OK, PREFIX + "/no-intercept" + APP);
    }
}
