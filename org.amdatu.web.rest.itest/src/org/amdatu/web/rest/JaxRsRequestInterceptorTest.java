/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest;

import org.amdatu.web.rest.jaxrs.JaxRsRequestInterceptor;
import org.amdatu.web.rest.jaxrs.RequestContext;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Application;
import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;

import static java.lang.Boolean.TRUE;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.*;

/**
 * Tests the usage of {@link JaxRsRequestInterceptorTest}
 */
public class JaxRsRequestInterceptorTest extends RegistrationTestBase {
    private static final String TEST_APPLICATION_NAME = "org.amdatu.web.rest.JaxRsRequestInterceptorTest";
    private static final String PREFIX = "/prefix";
    private static final String RESOURCE = "/test";

    @Path(RESOURCE)
    public static class TestResource {

        @GET
        @Path("{arg}")
        public String handleGetRequest(@PathParam("arg") Integer status) {
            return "test";
        }
    }

    public void testJaxRsRequestInterceptor() throws Exception {
        registerService(Application.class, new Application(),
                JAX_RS_APPLICATION_BASE, PREFIX,
                JAX_RS_NAME, TEST_APPLICATION_NAME);

        TestResource testResource = new TestResource();
        registerService(Object.class, testResource,
                JAX_RS_RESOURCE, TRUE,
                JAX_RS_APPLICATION_SELECT, createTargetFilter(JAX_RS_NAME, TEST_APPLICATION_NAME));

        AtomicReference<RequestContext> requestContextReference = new AtomicReference<>();

        registerService(JaxRsRequestInterceptor.class, requestContextReference::set,
                JAX_RS_EXTENSION, TRUE,
                JAX_RS_APPLICATION_SELECT, createTargetFilter(JAX_RS_NAME, TEST_APPLICATION_NAME));

        assertResponseCode(200, PREFIX + RESOURCE + "/" + 123);

        RequestContext requestContext;
        requestContext = requestContextReference.get();
        assertNotNull(requestContext);
        assertEquals("handleGetRequest", requestContext.getMethod().getName());
        assertEquals(TEST_APPLICATION_NAME, requestContext.getApplicationName());
        assertEquals(testResource, requestContext.getResource());
        assertEquals(Collections.singletonList(123), Arrays.asList(requestContext.getParams()));


        assertResponseCode(200, PREFIX + RESOURCE + "/" + 456);
        requestContext = requestContextReference.get();
        assertNotNull(requestContext);
        assertEquals("handleGetRequest", requestContext.getMethod().getName());
        assertEquals(TEST_APPLICATION_NAME, requestContext.getApplicationName());
        assertEquals(testResource, requestContext.getResource());
        assertEquals(Collections.singletonList(456), Arrays.asList(requestContext.getParams()));
    }

}
