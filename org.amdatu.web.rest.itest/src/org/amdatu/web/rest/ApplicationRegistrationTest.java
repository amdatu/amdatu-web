/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest;

import org.osgi.framework.Constants;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.http.context.ServletContextHelper;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Application;
import javax.ws.rs.ext.WriterInterceptor;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Collections;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Set;
import java.util.stream.Collectors;

import static java.lang.Boolean.TRUE;
import static javax.servlet.http.HttpServletResponse.SC_NOT_FOUND;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_CONTEXT;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_NAME;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_PATH;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_APPLICATION_SELECT;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_EXTENSION;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_NAME;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_RESOURCE;

/**
 * Tests the (de-)registration of applications
 */
public class ApplicationRegistrationTest extends RegistrationTestBase {

    private static final String TEST_APPLICATION_NAME = "org.amdatu.web.rest.ApplicationRegistrationTestApp";
    private static final String CONTEXT_NAME = "testContext";
    private static final String CONTEXT_PATH = "/context";
    private static final String PREFIX = "/prefix";
    private static final String BASE = "/base";
    private static final String SUB = "/base/sub";

    @Path(BASE)
    public static class TestBaseResource {
        @GET
        public String handleGetRequest() {
            return "base";
        }
    }

    @Path(SUB)
    public static class TestSubResource {
        @GET
        public String handleGetRequest() {
            return "sub";
        }
    }

    @Path("/")
    public static class TestResource {

        private String m_response;

        public TestResource(String m_response) {
            this.m_response = m_response;
        }

        @GET
        public String get() {
            return m_response;
        }
    }

    public void testDynamicApplication() throws Exception {
        assertResponseCode(HTTP_NOT_FOUND, BASE);

        ServiceRegistration<Application> applicationReg = registerService(Application.class, new Application(),
                JAX_RS_APPLICATION_BASE, "",
                JAX_RS_NAME, TEST_APPLICATION_NAME);

        registerService(Object.class, new TestBaseResource(),
                JAX_RS_RESOURCE, TRUE,
                JAX_RS_APPLICATION_SELECT, createTargetFilter(JAX_RS_NAME, TEST_APPLICATION_NAME));

        assertResponseCode(HTTP_OK, BASE);

        applicationReg.unregister();
        assertResponseCode(HTTP_NOT_FOUND, BASE);
    }

    public void testApplicationInContext() throws Exception {
        assertResponseCode(HTTP_NOT_FOUND, CONTEXT_PATH + BASE);

        registerService(ServletContextHelper.class, new ServletContextHelper() {
                },
                HTTP_WHITEBOARD_CONTEXT_PATH, CONTEXT_PATH,
                HTTP_WHITEBOARD_CONTEXT_NAME, CONTEXT_NAME
        );

        registerService(Application.class, new Application(),
                JAX_RS_APPLICATION_CONTEXT, CONTEXT_NAME,
                JAX_RS_APPLICATION_BASE, "",
                JAX_RS_NAME, TEST_APPLICATION_NAME);

        registerService(Object.class, new TestBaseResource(),
                JAX_RS_RESOURCE, TRUE,
                JAX_RS_APPLICATION_SELECT, createTargetFilter(JAX_RS_NAME, TEST_APPLICATION_NAME));


        // Register a second context with the same resource path

        registerService(ServletContextHelper.class, new ServletContextHelper() {

                },
                HTTP_WHITEBOARD_CONTEXT_PATH, "/secondContext",
                HTTP_WHITEBOARD_CONTEXT_NAME, "context2"
        );

        registerService(Application.class, new Application(),
                JAX_RS_APPLICATION_CONTEXT, "context2",
                JAX_RS_APPLICATION_BASE, "",
                JAX_RS_NAME, "application2"
        );

        registerService(Object.class, new TestBaseResource(),
                JAX_RS_RESOURCE, TRUE,
                JAX_RS_APPLICATION_SELECT, createTargetFilter(JAX_RS_NAME, "application2"));

        assertResponseCode(HTTP_OK, "/secondContext" + BASE);


        // Register an second application on the test context

        ServiceRegistration<Application> applicationReg = registerService(Application.class, new Application(),
                HTTP_WHITEBOARD_CONTEXT_NAME, CONTEXT_NAME,
                JAX_RS_APPLICATION_BASE, "",
                JAX_RS_NAME, "application3");

        registerService(Object.class, new TestSubResource(), JAX_RS_RESOURCE, TRUE,
                JAX_RS_APPLICATION_SELECT, createTargetFilter(JAX_RS_NAME, "application3"));

        assertResponseCode(HTTP_NOT_FOUND, CONTEXT_PATH + SUB);
        applicationReg.unregister();


        registerService(Application.class, new Application(),
                Constants.SERVICE_RANKING, Integer.MAX_VALUE,
                JAX_RS_APPLICATION_CONTEXT, CONTEXT_NAME,
                JAX_RS_APPLICATION_BASE, "",
                JAX_RS_NAME, "application3"
        );

        assertResponseCode(HTTP_OK, CONTEXT_PATH + SUB);
        assertResponseCode(HTTP_NOT_FOUND, CONTEXT_PATH + BASE);
    }

    public void testPrefixedApplicationRegistration() throws Exception {
        String prefixedBase = PREFIX + BASE;
        assertResponseCode(HTTP_NOT_FOUND, prefixedBase);

        ServiceRegistration<Application> applicationReg = registerService(Application.class, new Application(),
                JAX_RS_APPLICATION_BASE, PREFIX,
                JAX_RS_NAME, TEST_APPLICATION_NAME);

        registerService(Object.class, new TestBaseResource(),
                JAX_RS_RESOURCE, TRUE,
                JAX_RS_APPLICATION_SELECT, "(" + JAX_RS_NAME + "=" + TEST_APPLICATION_NAME + ")");

        assertResponseCode(HTTP_OK, prefixedBase);

        applicationReg.unregister();
        assertResponseCode(HTTP_NOT_FOUND, prefixedBase);
    }

    public void testStaticApplication() throws Exception {
        assertResponseCode(HTTP_NOT_FOUND, PREFIX + BASE);

        registerService(Application.class, new Application() {
                    @Override
                    public Set<Object> getSingletons() {
                        return Collections.singleton(new TestBaseResource());
                    }
                },
                JAX_RS_APPLICATION_BASE, PREFIX,
                JAX_RS_NAME, TEST_APPLICATION_NAME);

        assertResponseCode(HTTP_OK, PREFIX + BASE);
    }

    public void testShadowedApplication() throws Exception {
        assertResponseCode(HTTP_NOT_FOUND, PREFIX);

        registerService(Application.class, new Application() {
                    @Override
                    public Set<Object> getSingletons() {
                        return Collections.singleton(new TestResource("shadowed"));
                    }
                },
                JAX_RS_APPLICATION_BASE, PREFIX,
                JAX_RS_NAME, TEST_APPLICATION_NAME);

        assertResponseContent("shadowed", PREFIX);

        ServiceRegistration<Application> shadowing = registerService(Application.class, new Application() {
                    @Override
                    public Set<Object> getSingletons() {
                        return Collections.singleton(new TestResource("shadowing"));
                    }
                },
                JAX_RS_APPLICATION_BASE, PREFIX,
                JAX_RS_NAME, TEST_APPLICATION_NAME,
                Constants.SERVICE_RANKING, 1);

        assertResponseContent("shadowing", PREFIX);

        shadowing.unregister();
        assertResponseContent("shadowed", PREFIX);
    }

    public void testResourceTargetingMultipleApplications() throws Exception {
        registerApplication("app1");
        registerApplication("app2");

        registerTestResource("test-resource", "*");

        assertResponseContent("test-resource", "/app1");
        assertResponseContent("test-resource", "/app2");
    }

    public void testApplicationWithExtensionSelect() throws Exception {
        application("app1").register();
        application("app2").withExtensions("ext1").register();

        registerTestResource("test-resource", "*");

        assertResponseContent("test-resource", "/app1");
        assertResponseCode(SC_NOT_FOUND, "/app2");

        // Now register the missing extension targeting only the app that depends on it
        ServiceRegistration<?> ext1 = extension("ext1").forApplications("app2").register();

        assertResponseContent("test-resource", "/app1");
        assertResponseContent("[ext1]test-resource", "/app2");

        // Unregister the extension
        ext1.unregister();

        assertResponseContent("test-resource", "/app1");
        assertResponseCode(SC_NOT_FOUND, "/app2");

        // Register the extension again no targeting both applications
        ext1 = extension("ext1").forApplications("app1", "app2").register();

        assertResponseContent("[ext1]test-resource", "/app1");
        assertResponseContent("[ext1]test-resource", "/app2");

        // Unregister the extension
        ext1.unregister();

        // Now register the extension but make it depend on "ext2"
        ext1 = extension("ext1").forApplications("app1", "app2").withExtensions("ext2").register();

        // Extension should not be available yet
        assertResponseContent("test-resource", "/app1");
        assertResponseCode(SC_NOT_FOUND, "/app2");

        // Now register the missing extension targeting only app2
        ServiceRegistration<?> ext2 = extension("ext2").forApplications("app2").register();
        assertResponseContent("test-resource", "/app1");
        assertResponseContent("[ext1][ext2]test-resource", "/app2");

        ext2.unregister();
        assertResponseContent("test-resource", "/app1");
        assertResponseCode(SC_NOT_FOUND, "/app2");

        // Now register the missing extension targeting both apps
        ext2 = extension("ext2").forApplications("app1", "app2").register();
        assertResponseContent("[ext1][ext2]test-resource", "/app1");
        assertResponseContent("[ext1][ext2]test-resource", "/app2");


        ext1.unregister();
        assertResponseContent("[ext2]test-resource", "/app1");
        assertResponseCode(SC_NOT_FOUND, "/app2");

        // registering ext1 again changes the order as there is no service ranking so it'll be registration order
        ext1 = extension("ext1").forApplications("app1", "app2").withExtensions("ext2").register();
        assertResponseContent("[ext2][ext1]test-resource", "/app1");
        assertResponseContent("[ext2][ext1]test-resource", "/app2");
    }

    public void testExtensionAvailableBeforeApplication() throws Exception {
        ServiceRegistration<?> ext1 = extension("ext1").forApplications("app1", "app2").register();

        ServiceRegistration<Application> app1 = application("app1").register();
        ServiceRegistration<Application> app2 = application("app2").withExtensions("ext1").register();

        registerTestResource("test-resource", "*");

        assertResponseContent("[ext1]test-resource", "/app1");
        assertResponseContent("[ext1]test-resource", "/app2");

        app1.unregister();
        assertResponseCode(SC_NOT_FOUND,  "/app1");
        assertResponseContent("[ext1]test-resource", "/app2");

        app1 = application("app1").register();
        assertResponseContent("[ext1]test-resource", "/app1");
        assertResponseContent("[ext1]test-resource", "/app2");
    }

    public void testCircularExtensionDependency() throws Exception {
        application("app2").withExtensions("ext1").register();

        registerTestResource("test-resource", "*");

        assertResponseCode(SC_NOT_FOUND, "/app1");

        // Now register the missing extension targeting only the app that depends on it
        ServiceRegistration<?> ext1 = extension("ext1")
                .withExtensions("ext2")
                .register();

        ServiceRegistration<?> ext2 = extension("ext2")
                .withExtensions("ext1")
                .register();


    }

    private ServiceRegistration<Application> registerApplication(String name) {
        return registerService(Application.class, new Application(),
                JAX_RS_APPLICATION_BASE, "/" + name,
                JAX_RS_NAME, name);
    }

    private ServiceRegistration<?> registerTestResource(String name, String application) {
        return registerService(TestResource.class, new TestResource(name),
                JAX_RS_RESOURCE, TRUE,
                JAX_RS_NAME, name,
                JAX_RS_APPLICATION_SELECT, createTargetFilter(JAX_RS_NAME, application));
    }

    private ServiceRegistration<?> registerExtension(String name) {
        return registerService(WriterInterceptor.class, context -> {
                    PrintWriter printWriter = new PrintWriter(context.getOutputStream());
                    printWriter.write("[" + name + "]");
                    printWriter.flush();
                    context.proceed();
                },
                JAX_RS_EXTENSION, TRUE,
                JAX_RS_NAME, name);
    }

    private ApplicationRegistrationBuilder application(String name) {
        return new ApplicationRegistrationBuilder(name);
    }

    private ExtensionRegistrationBuilder extension(String name) {
        return new ExtensionRegistrationBuilder(name);
    }

    class ApplicationRegistrationBuilder {

        private String name;
        private Application application;
        private String[] extensionSelect;
        private String path;

        public ApplicationRegistrationBuilder(String name) {
            this.name = name;
        }

        public ApplicationRegistrationBuilder withPath(String path) {
            this.path = path;

            return this;
        }

        public ApplicationRegistrationBuilder withExtensions(String... extensionNames) {

            if (extensionNames != null && extensionNames.length > 0) {
                extensionSelect = Arrays.stream(extensionNames)
                        .map(name -> createTargetFilter(JAX_RS_NAME, name))
                        .toArray(String[]::new);
            } else {
                extensionSelect = null;
            }
            return this;
        }

        public ApplicationRegistrationBuilder withApplication(Application application) {
            this.application = application;
            return this;
        }

        public ServiceRegistration<Application> register() {
            Dictionary<String, Object> dict = new Hashtable<>();
            dict.put(JAX_RS_APPLICATION_BASE, path != null ? path : "/" + name);
            dict.put(JAX_RS_NAME, name);

            if (extensionSelect != null) {
                dict.put(JaxrsWhiteboardConstants.JAX_RS_EXTENSION_SELECT, extensionSelect);
            }


            return registerService(Application.class, new Application(), dict);
        }
    }

    class ExtensionRegistrationBuilder {

        private String name;
        private String applicationSelect;
        private String[] extensionSelect;

        public ExtensionRegistrationBuilder(String name) {
            this.name = name;
        }

        public ExtensionRegistrationBuilder forApplications(String... applicationNames) {

            if (applicationNames != null && applicationNames.length > 0) {
                applicationSelect = "(|" + Arrays.stream(applicationNames)
                        .map(name -> createTargetFilter(JAX_RS_NAME, name))
                        .collect(Collectors.joining()) + ")";
            } else {
                applicationSelect = null;
            }
            return this;
        }

        public ExtensionRegistrationBuilder withExtensions(String... extensionNames) {

            if (extensionNames != null && extensionNames.length > 0) {
                extensionSelect = Arrays.stream(extensionNames)
                        .map(name -> createTargetFilter(JAX_RS_NAME, name))
                        .toArray(String[]::new);
            } else {
                extensionSelect = null;
            }
            return this;
        }


        public ServiceRegistration<?> register() {
            Dictionary<String, Object> dict = new Hashtable<>();
            dict.put(JAX_RS_EXTENSION, TRUE);
            dict.put(JAX_RS_NAME, name);

            if (applicationSelect != null) {
                dict.put(JAX_RS_APPLICATION_SELECT, applicationSelect);
            }

            if (extensionSelect != null) {
                dict.put(JaxrsWhiteboardConstants.JAX_RS_EXTENSION_SELECT, extensionSelect);
            }

            return registerService(WriterInterceptor.class, context -> {
                        PrintWriter printWriter = new PrintWriter(context.getOutputStream());
                        printWriter.write("[" + name + "]");
                        printWriter.flush();
                        context.proceed();
                    },
                    dict);
        }
    }

}
