/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.demo.rest.application;

import org.amdatu.web.rest.doc.Description;
import org.amdatu.web.rest.doc.ResponseMessage;
import org.amdatu.web.rest.doc.ResponseMessages;
import org.apache.felix.dm.annotation.api.Component;
import org.osgi.service.jaxrs.whiteboard.propertytypes.JaxrsApplicationSelect;
import org.osgi.service.jaxrs.whiteboard.propertytypes.JaxrsResource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import static org.amdatu.web.demo.rest.application.DemoApplication.DEMO_APPLICATION;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_NAME;


/**
 * Resource demonstrating how to add resources to an application by registering them as a service in the OSGi service registry
 */
@Path("service")
@Description("Demonstrates how to add additional resources to an application")
@Component(provides = Object.class)
@JaxrsResource
@JaxrsApplicationSelect("(" + JAX_RS_NAME + "=" + DEMO_APPLICATION + ")")
public class WhiteboardResource {

    @Context
    private Application application;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Description("Will return a friendly message from your server.")
    @ResponseMessages(@ResponseMessage(code = 200, message = "Friendly text message"))
    public String helloPlain() {
        return String.format("I'm registered to as a service in the OSGi service registry and added to '%s'", application.getClass());
    }

}
