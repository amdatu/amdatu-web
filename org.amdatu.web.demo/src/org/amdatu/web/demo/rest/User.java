/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.demo.rest;

import javax.ws.rs.DefaultValue;

import org.amdatu.web.rest.doc.Description;

/**
 * Represents a user with a first and last name.
 */
public class User {

    @Description("First name of the user.")
    private String first;

    @DefaultValue("Doe")
    @Description("Last name of the user.")
    private String last;

    public User() {
    }

    public User(String first, String last) {
        this.first = first;
        this.last = last;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    @Override
    public String toString() {
        return "User[" + first + " " + last + "]";
    }
}