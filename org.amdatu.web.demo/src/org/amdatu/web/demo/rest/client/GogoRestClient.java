/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.demo.rest.client;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.amdatu.web.demo.rest.User;
import org.amdatu.web.demo.rest.UserResource;
import org.amdatu.web.rest.jaxrs.client.AmdatuClientBuilder;
import org.amdatu.web.rest.jaxrs.client.AmdatuClient;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.dm.annotation.api.Start;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

@Component(provides = Object.class)
@Property(name = "osgi.command.scope", value = "demo")
@Property(name = "osgi.command.function", value = {"hello", "user"})
public class GogoRestClient {

    @ServiceDependency
    private volatile ClientBuilder m_clientBuilder;

    @ServiceDependency
    private volatile AmdatuClientBuilder m_amdatuClientBuilder;

    @Start
    protected final void start() {
        m_amdatuClientBuilder.register(JacksonJsonProvider.class);
    }

    public void hello() {
        Client client = m_clientBuilder.build();

        WebTarget target = client.target("http://localhost:8080/rest/hello");
        Response response = target.request().get();
        String value = response.readEntity(String.class);
        int status = response.getStatus();
        response.close();

        System.out.println(String.format("Response (status: %s): '%s'", status, value));
    }

    public void user() {
        UserResource userResource = createUserResourceProxy();
        System.out.println(String.format("Get user response: '%s'", userResource.getUser()));
    }

    public void user(String first, String last) {
        UserResource userResource = createUserResourceProxy();

        User user = new User(first, last);
        System.out.println(String.format("Set user response: '%s'", userResource.setUser(user)));
    }

    /**
     * Create a JAX-RS client proxy based on the {@link UserResource} interface
     * @return
     */
    private UserResource createUserResourceProxy() {
        AmdatuClient client = m_amdatuClientBuilder.build();

        return client.target("http://localhost:8080/rest")
                .proxyBuilder(UserResource.class)
                .defaultConsumes(MediaType.APPLICATION_JSON).build();
    }

}
