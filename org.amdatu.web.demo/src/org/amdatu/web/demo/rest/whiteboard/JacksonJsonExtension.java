/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.demo.rest.whiteboard;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import org.apache.felix.dm.annotation.api.Component;
import org.osgi.service.jaxrs.whiteboard.propertytypes.JaxrsExtension;
import org.osgi.service.jaxrs.whiteboard.propertytypes.JaxrsMediaType;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.MessageBodyWriter;

/**
 * Extension that provides support the the application/json media type based on the {@link JacksonJsonProvider}
 */
@Component(provides = {MessageBodyWriter.class, MessageBodyReader.class})
@JaxrsExtension
@JaxrsMediaType(MediaType.APPLICATION_JSON)
public class JacksonJsonExtension extends JacksonJsonProvider {

}
