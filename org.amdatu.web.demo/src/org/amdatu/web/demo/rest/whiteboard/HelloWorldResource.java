/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.demo.rest.whiteboard;

import org.amdatu.web.rest.doc.Description;
import org.amdatu.web.rest.doc.ResponseMessage;
import org.amdatu.web.rest.doc.ResponseMessages;
import org.amdatu.web.rest.doc.ReturnType;
import org.apache.felix.dm.annotation.api.Component;
import org.osgi.service.jaxrs.whiteboard.propertytypes.JaxrsResource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Path("hello")
@Description("Demonstrates a REST endpoint where the JAX-RS annotations are on interface methods.")
@Component(provides = Object.class)
@JaxrsResource
public class HelloWorldResource {

    @GET
    @Description("Will return a friendly message from your server.")
    @ResponseMessages(@ResponseMessage(code = 200, message = "Friendly text message"))
    @Produces(MediaType.TEXT_PLAIN)
    public String helloPlain() {
        return "hello world";
    }

    @GET
    @Path("response-with-headers")
    @Description("Will return a friendly message from your server with headers.")
    @ResponseMessages(@ResponseMessage(code = 200, message = "Friendly message with extra header"))
    @ReturnType(String.class)
    @Produces(MediaType.TEXT_PLAIN)
    public Response helloResponse() {
        return Response.ok("hello world").header("Link", "http://www.amdatu.org/").build();
    }

    @GET
    @Path("with-uri-info")
    @Description("Returns a message with some context information")
    @Produces(MediaType.TEXT_PLAIN)
    public String helloUriInfo(@Context UriInfo uriInfo) {
        return "Hello world from " + uriInfo.getAbsolutePath();
    }

    @GET
    @Path("error-exception")
    @Description("Returns a message with some context information")
    @Produces(MediaType.TEXT_PLAIN)
    public String errorException(@Context UriInfo uriInfo) {
        throw new RuntimeException("Intentional exception");
    }
}
